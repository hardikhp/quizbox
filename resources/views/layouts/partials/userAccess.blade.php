<form id="course-access" method="post" class="form-horizontal" action="">
    <div class="form-group">
        <label class="control-label">Users</label>

        <select name="users[]" class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Choose ...">

            <option value="">Select User Name</option>
            @foreach($users as $user)
                <option @if(in_array($user->id, $user_ids))selected="selected"@endif value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
        <input type="hidden" name="course_id" value="{{$course_id}}">
    </div>
    <div class="form-group">
    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
    </div>

</form>

<script type="text/javascript">
$('#course-access').on('submit',function(e){
    e.preventDefault(e);
    $("#AccessModal").modal('hide');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
    $.ajax({
        type:'POST',
        url:'{{route('users.access')}}',
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
        },
        error: function(data){

        }
    });
});
</script>