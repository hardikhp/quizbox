<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Quizbox</title>
    @if(isset($module->meta_description))
        <meta content="{{ $module->meta_description }}" name="description" />
    @else
        <meta content="QuizBox" name="description" />
    @endif
    @if(isset($module->meta_keywords))
        <meta content="{{ $module->meta_keywords }}" name="keywords" />
    @else
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    @endif
    <link rel="shortcut icon" href="{{ url('/assets/images/favicon.ico') }}">

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/plugins/morris/morris.css') }}">
    <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/assets/css/custom.css') }}" rel="stylesheet" type="text/css">

    @yield('PageCSS')
</head>
<body>
    <!-- Begin page -->
    <div id="wrapper">

        @include('layouts.partials.topBar')

        @include('layouts.partials.leftBar')

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            @yield('content')

            
        </div>
        <footer class="footer">
            © {{date('Y')}} {{ config('app.name', 'QuizBox') }}, Hardik & Manan - All rights reserved </span>.
        </footer>


        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->
        

    <!-- jQuery  -->
    <script src="{{ url('/assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('/assets/js/metisMenu.min.js') }}"></script>
    <script src="{{ url('/assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ url('/assets/js/waves.min.js') }}"></script>

    <script src="{{ url('/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

    <!--Morris Chart-->
    <script src="{{ url('/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ url('/plugins/raphael/raphael-min.js') }}"></script>
    <script src="{{ url('/assets/pages/dashboard.js') }}"></script>

    <!-- App js -->
    <script src="{{ url('/assets/js/app.js') }}"></script>
    @yield('PageJS')
</body>
</html>