@extends('layouts.app')

@section('PageCSS')
<!-- DataTables -->
<link href="{{url('/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

    	<div class="row">
    		@if(Session::has('message'))
			    <div class="card-body">
			        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible" role="alert">
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                 <span aria-hidden="true">×</span>
			            </button>{{ Session::get('message') }}
			        </div>
			    </div>
			@endif
    	</div>

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Results</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Results</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                	<div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Results</h4>
                    </div>
                    <div class="card-body">

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Correct Answers</th>
                                <th>InCorrect Answers</th>
                                <th>Percentage</th>
                                <th>Result</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->

</div> <!-- content -->
<div class="container">


  

  <!-- Modal -->
  
@endsection

@section('PageJS')
<!-- Required datatable js -->
<script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#datatable').DataTable({
	        processing: true,
	        serverSide: true,
            searching: false,
	        ajax: '{{ route('results.json',['id' => $id]) }}',
	        columns: [
                { data: 'user_id', name: 'user_id', "orderable": false },
                { data: 'correct', name: 'correct' },
                { data: 'Incorrect', name: 'Incorrect' },
                { data: 'Percentage', name: 'Percentage' },
                { data: 'Result', name: 'Result' },
                { data: 'Action', name: 'Action' },
            ]
	    });
	});
</script>
<script>
    function modelOpen(userid){
        var user_id=userid;
        
        var url = window.location.pathname;
        var video_id = url.substring(url.lastIndexOf('/') + 1);
       
        $.ajax({
                type:'get',
                url:"{{ route('results.modelData') }}",
                data:{
                        userid: user_id,
                        video_id:video_id,
                    },
               
               success:function(data) {
  
               document.getElementById("modelResult").innerHTML=data;
               $('#myModal').modal('show');
               }
            });
    }
</script>
<div class="modal fade quizresult" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h4 class="modal-title">Quiz Result</h4>
                </div>
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
            <div class="modal-body">
                <div id="modelResult">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>

@endsection