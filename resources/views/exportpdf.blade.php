<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{public_path('assets/css/style.css')}}">
    <style>
        body{font-family:'Nunito', sans-serif;font-size: 14px;line-height:16px;font-weight:500;color:#000;letter-spacing: 0.3px;}
        table { width: 100%; }
        span.btm { display: block; font-size: 11px; line-height: 13px; text-transform: capitalize;}
        table tr th.heading { font-weight: bold; border: 0; }
        table thead tr th { font-weight: normal; border-bottom: 1px solid #999; }
        .light-black { color: #222;}
        .alert-success { color: #2e6740; background-color: #d6f6e0; font-weight: 600;}
        .alert-danger { color: #ec536c; background-color: #fbdde2; font-weight: 600;}
        .alert-success span { background: rgba(43, 121, 69, 0.7);}
        .alert-danger span { background: rgba(236, 83, 108, 0.7);}
        .answer-status span { margin-left: 0; margin-right: 10px; width: 20px; height: 20px;}
        .answer-status .alert { padding: .5rem .5rem; margin-bottom: 0;}
        .user-detail-box { padding: 10px 15px; }
        .user-detail-box h5 { font-size: 14px; margin-bottom: 5px; }
        .user-detail-box h4 { font-size: 20px; }
        h4 { margin-bottom: 7px; }
    </style>
</head>
<body>
    <table style="border: 0; background-color: #17a2b8; padding: 10px 20px 0px; margin-bottom: 20px;">
        <tbody>
            <tr>
                <td><h1 style="color: #fff;">QuizBox</h1></td>
            </tr>
        </tbody>
    </table>
    <table style="margin-bottom: 0; color: #222;">
        <tbody>
            <tr>
                <td><h2>User Details:</h2></td>
            </tr>
        </tbody>
    </table>
    <table style="margin-bottom: 0;">
        <tbody>
            <tr>
                <td>
                	<div class="card mini-stat bg-primary user-detail-box">
                        <div class="text-white">
                            <h5 class="text-uppercase mb-3">User Name</h5>
                            <h4 class="mb-4">{{$username}}</h4>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin-bottom: 0; color: #222;">
        <tbody>
            <tr>
                <td><h2>Quiz Result:</h2></td>
            </tr>
        </tbody>
    </table>
    <table style="margin-bottom: 0;">
        <tbody>
            <tr>
                <td width="25%">
                	<div class="card mini-stat bg-primary user-detail-box">
                        <div class="text-white">
                            <h5 class="text-uppercase mb-3">Correct Answers</h5>
                            <h4 class="mb-4">{{$correct}}</h4>
                        </div>
                    </div>
                </td>
                <td width="25%">
                	<div class="card mini-stat bg-primary user-detail-box">
                        <div class="text-white">
                            <h5 class="text-uppercase mb-3">Incorrect Answers</h5>
                            <h4 class="mb-4">{{$incorrect}}</h4>
                        </div>
                    </div>
                </td>
                <td width="25%">
                	<div class="card mini-stat bg-primary user-detail-box">
                        <div class="text-white">
                            <h5 class="text-uppercase mb-3">Percentage</h5>
                            <h4 class="mb-4">{{$percentage}} %</h4>
                        </div>
                    </div>
                </td>
                <td width="25%">
                	<div class="card mini-stat bg-primary user-detail-box">
                        <div class="text-white">
                            <h5 class="text-uppercase mb-3">Result</h5>
                            <h4 class="mb-4">{{$results}}</h4>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    @if(isset($retry_count) && $retry_count > 0)
        <h4 class="mt-0 m-b-30 header-title bold-font tryquiz-count">Quiz Try: {{$retry_count}}</h4>
    @endif
    @foreach($model_data as $model)
    	@if($model->is_correct==1)
		    <table>
		      <tbody>
		            <tr>
		                <td><h4 class="mt-0 m-b-10 header-title light-black bold-font">Q. {{ $model->question }}</h4></td>
		            </tr>
		            <tr>
		                <td>
		                	<div class="answer-status">
								<div class="alert alert-success" role="alert">
									<img src="{{public_path('assets/images/check.png')}}" alt="user" class="rounded-circle" width="15" style="margin-top: 3px; margin-right: 5px;">
									A. {{ $model->answer }}
								</div>
							</div>
						</td>
		            </tr>
		      </tbody>
		    </table>
	    @else
	    	<table>
		      <tbody>
		            <tr>
		                <td><h4 class="mt-0 m-b-10 header-title light-black bold-font">Q. {{ $model->question }}</h4></td>
		            </tr>
		            <tr>
		                <td>
		                	<div class="answer-status">
								<div class="alert alert-danger" role="alert">
									<img src="{{public_path('assets/images/wrong.png')}}" alt="user" class="rounded-circle" width="15" style="margin-top: 3px; margin-right: 5px;">
									A. {{ $model->answer }}
								</div>
							</div>
		                </td>
		            </tr>
		      </tbody>
		    </table>
		@endif
	@endforeach
</body>