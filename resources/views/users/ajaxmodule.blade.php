@foreach($courses as $course)
    <div class="card-header card-default bg-danger">
        <h4 class="mt-0 mb-0 header-title text-white">{{$course['name']}}</h4>
    </div>
    <div class="row">
        <div class="col-xl-12 col-md-12">
            <div class="directory-card m-b-20">
                <div class="card-body">
                    <div class="directory-content">
                        {!! ($course['description']) !!}
                    </div>
                    <div class="row">
                        @foreach($modules as $module)
                            @if($course['id'] == $module['course_id'])
                            <div class="col-lg-4 col-sm-6">
                                <div class="card m-b-30">
                                    @if($module['video'] != "")
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <span class= "pip">
                                            <video width="100%" height="240" controls controlsList="nodownload">  
                                            <source src="{{ $module['video'] }}" type="video/mp4">  
                                            Your browser does not support the video tag.
                                            </video> 
                                            </span>
                                        </div>
                                    @endif
                                    <div class="card-body">
                                        <h4 class="card-title font-16 mt-0"><a href="{{route('users.modules.show', ['id' => $module['id']])}}" class="read-more">{{ $module['name'] }}</a></h4>
                                        {!! str_limit($module['description'], 100) !!}
                                        @if (strlen($module['description']) > 100)
                                          <a href="{{route('users.modules.show', ['id' => $module['id']])}}" class="read-more">Read More</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach