@extends('layouts.appUser')

@section('PageCSS')
<!-- DataTables -->
<link href="{{url('/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

    	<div class="row">
    		@if(Session::has('message'))
			    <div class="card-body">
			        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible" role="alert">
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                 <span aria-hidden="true">×</span>
			            </button>{{ Session::get('message') }}
			        </div>
			    </div>
			@endif
    	</div>

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Courses</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('users.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Courses</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                	<div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Courses</h4>
                    </div>
                    <div class="card-body">

                        <table id="datatable1" class="dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Added By</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')
<!-- Required datatable js -->
<script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<script src="{{url('/plugins/select2/js/select2.min.js')}}"></script>

@endsection