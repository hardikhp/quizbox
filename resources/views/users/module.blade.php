@extends('layouts.appUser')

@section('PageCSS')
<!-- DataTables -->
<link href="{{url('/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

    	<div class="row">
    		@if(Session::has('message'))
			    <div class="card-body">
			        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible" role="alert">
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                 <span aria-hidden="true">×</span>
			            </button>{{ Session::get('message') }}
			        </div>
			    </div>
			@endif
    	</div>

        <div class="row m-b-30">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Modules</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('users.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Modules</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="module-section" id="module-section">
            @foreach($courses as $course)
                <div class="card-header card-default bg-danger">
                    <h4 class="mt-0 mb-0 header-title text-white">{{$course['name']}}</h4>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="directory-card m-b-20">
                            <div class="card-body">
                                <div class="directory-content">
                                    {!! ($course['description']) !!}
                                </div>
                                <div class="row">
                                    @foreach($modules as $module)
                                        @if($course['id'] == $module['course_id'])
                                            <div class="col-lg-4 col-sm-6">
                                                <div class="card m-b-30">
                                                    @if($module['video'] != "")
                                                        <div class="embed-responsive embed-responsive-16by9" >
                                                            <span class= "pip">
                                                                <a href="{{route('users.modules.show', ['id' => $module['id']])}}" class="read-more">
                                                            <video width="100%" height="240" class="modulevid" controlsList="nodownload" controls>  
                                                            <source src="{{ $module['video'] }}" type="video/mp4">  
                                                            Your browser does not support the video tag.
                                                            </video></a> 
                                                            </span>
                                                        </div>
                                                    @endif
                                                    <div class="card-body">
                                                        <h4 class="card-title font-16 mt-0"><a href="{{route('users.modules.show', ['id' => $module['id']])}}" class="read-more">{{ $module['name'] }}</a></h4>
                                                        {!! str_limit($module['description'], 100) !!}
                                                        @if (strlen($module['description']) > 100)
                                                          <a href="{{route('users.modules.show', ['id' => $module['id']])}}" class="read-more">Read More</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')
<!-- Required datatable js -->
<script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<script type="text/javascript">
    var pageNumber = 3;
    $(window).scroll(function() {
        if({{$coursesCount}} > pageNumber)
        {
            if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                $.ajax({
                    url: '{{ route('users.modules') }}',
                    method : "POST",
                    data : {id:pageNumber, _token:"{{csrf_token()}}"},
                    dataType : "text",
                    success : function(data){
                        pageNumber +=3;
                            if(data.length == 0)
                            {
                            }
                            else{
                                $('#module-section').append(JSON.parse(data).html);
                            }
                    },error: function(data){

                    },
                })
            }
        }
    });

    $(document).ready(function(){
        $(".modulevid").attr('controls', false);
    });

/*var vid = document.getElementById("myVideo");
vid.oncanplay = function() {
  //   confirm("Press a button!");
  alert("Can start playing video");
};*/

</script>
@endsection