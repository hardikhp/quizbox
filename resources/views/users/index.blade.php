@extends('layouts.appUser')

@section('PageCSS')
<!-- DataTables -->
<link href="{{url('/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
    	<div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            Welcome to Quizbox
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-buffer float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Total Modules</h6>
                            <h4 class="mb-4">{{$totalModules}}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi-buffer float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Completed Modules</h6>
                            <h4 class="mb-4">{{$attendModules}}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat bg-primary">
                    <div class="card-body mini-stat-img">
                        <div class="mini-stat-icon">
                            <i class="mdi mdi mdi-buffer float-right"></i>
                        </div>
                        <div class="text-white">
                            <h6 class="text-uppercase mb-3">Uncompleted Modules</h6>
                            <h4 class="mb-4">{{$notattendedModules}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')
<!-- Required datatable js -->
<script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

@endsection