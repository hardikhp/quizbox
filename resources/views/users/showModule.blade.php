@extends('layouts.appUser')
@section('content')
<!-- Start content -->
<div class="content modules-content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <h4 class="page-title">Module</h4>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('modules')}}">Modules</a></li>
            <li class="breadcrumb-item active">{{$module->name}}</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- end row -->
    <div class="row">
      <div class="col-12">
        <div class="card m-b-20">
          <div class="card-header card-default">
            <h4 class="mt-0 mb-0 header-title">{{$module->name}}</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-xl-12 col-md-12 m-b-10">
                <div class="directory-content pd-lr-0 pd-tb-0">
                  <h3 class="font-20">Course : {{$course->name}}</h3>
                  <div class="progress m-b-10" style="height: 2px;">
                    <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row m-b-20">
              <div class="col-xl-6 col-md-6">
                <div class="directory-card m-b-10">
                  <div>
                    <div class="directory-content pd-lr-0 pd-tb-0">
                      <h5 class="mt-0 font-16">{{$module->name}}</h5>
                    </div>
                  </div>
                </div>
              </div> <!-- end col -->
              <div class="col-xl-12 col-md-12">
                <div class="directory-card m-b-10">
                  <div>
                    <div class="directory-content pd-lr-0">
                      @if($module->video != "")
                        <div class="embed-responsive embed-responsive-16by9 modulevidbox">
                          <span class= "pip">
                          <video id="modulevid" width="100%" height="" onended="videoEnded()" controlsList="nodownload" controls >  
                          <source src="{{ $module->video }}" type="video/mp4">  
                          Your browser does not support the video tag.
                          </video> 
                          </span>
                        {{--  @if(isset($attendQuesCount) && $attendQuesCount == 0) --}}
                            <div class="queans-container" id="queans-container">
                              <div class="row height100">
                                <div class="col-xl-12 col-md-12 height100">
                                  <div class="card directory-card m-b-20 height100">
                                    <div class="queans height100">
                                      @if(isset($module->questions) && $module->questions->count() >= 3)
                                        @php $quenum = 1 @endphp
                                        @foreach($module->questions->random(3) as $question)
                                          @php $queindex = $loop->index @endphp
                                          <form role="form" action="" method="post" class="queans-form">
                                            <div class="directory-content pd-bt-20">
                                              <div class="card-header card-default bg-danger m-b-20">
                                                <h4 class="mt-0 mb-0 header-title text-white">Quiz {{$quenum}}/3</h4>
                                              </div>
                                              <h4 class="mt-0 header-title light-red bold-font">{{$question->question}}</h4>
                                              @if(isset($question->answers) && $question->answers !== null)
                                                @foreach($question->answers as $anskey => $ansofque)
                                                  <div class="btn-group answers-row" data-toggle="buttons">
                                                    <label class="btn btn-outline-primary btn-block waves-effect waves-light radansbox" onclick="selectedAnswer(this);">
                                                        <input type="radio" name="answer_id" value="{{$ansofque->id}}"> {{$ansofque->answer}}
                                                    </label>
                                                  </div>
                                                @endforeach
                                              @endif
                                              <input type="hidden" name="question_id" value="{{$question->id}}">
                                              <input type="hidden" name="retry_quiz" value="{{$retry_quiz}}">
                                              <div class="input-error alert alert-danger alert-dismissible fade show" role="alert">Please select any answer.</div>
                                            </div>
                                          </form>
                                          @php $quenum++ @endphp
                                        @endforeach
                                        <div class="directory-content pd-bt-20 btn-box">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-submit" onclick="submitAnswer(this);">Submit</button>
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-next" onclick="nextQuestion(this);" disabled>Next</button>
                                        </div>
                                      @else
                                      <div class="card-header card-default bg-danger m-b-20">
                                          <h4 class="mt-0 mb-0 header-title text-white">There are not any quiz found for this module.</h4>
                                      </div>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        {{--  @endif --}}
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div> <!-- end col -->
            </div>
            <div class="row">
              <div class="col-xl-12 col-md-12">
                <div class="directory-content pd-lr-0 pd-tb-0">
                  {!! ($module->description) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-12 col-md-12">
                <div class="card directory-card m-b-20">
                  <div>
                    <div class="directory-content p-4">
                      {!! ($module->description) !!}
                    </div>
                  </div>
                </div>
              </div> <!-- end col -->
            </div>
          </div>
        </div>
      </div> <!-- end col -->
    </div> <!-- end row -->
  </div> <!-- container-fluid -->
</div> <!-- content -->
@endsection

@section('PageJS')
<script type="text/javascript">
  $(document).ready(function(){
    $('#queans-container').hide();
    {{-- @if($attendQuesCount == 0) --}}
        $('#modulevid').on('ended',function(){
            $(this).hide();
            $('#queans-container').show();
        });
    {{-- @endif --}}

    $('.queans .queans-form:first-child').fadeIn('slow');
    $('.queans .queans-form:first-child').addClass('activeform');

  });
  function selectedAnswer(selectedAnswerInput){
    $(selectedAnswerInput).closest('.queans-form').find('.radansbox').removeClass('checked');
    $(selectedAnswerInput).addClass('checked');
  }

  function nextQuestion(nextQuestion){
    var parent_fieldset = $(nextQuestion).parents('.queans').find('.answer-status-box');
    var activeform = $(nextQuestion).closest('.queans').find('.queans-form.activeform');

    $(nextQuestion).parents('.queans').find('button.btn-submit').removeAttr('disabled');
    $(nextQuestion).parents('.queans').find('button.btn-next').attr('disabled', true);
    
    $(parent_fieldset).remove();
    $(activeform).fadeIn();
  }

  function submitAnswer(form) {

    var answercount = $(form).closest('.queans').find('.activeform .answers-row label.checked').length;
    var activeform = $(form).closest('.queans').find('.queans-form.activeform');
    var queansform = $(form).closest('.queans').find('.queans-form');
    var numforms = $(form).closest('.queans').find('.queans-form').length;
    var activeformindex = $('.queans-form.activeform').index() + 1;

    if(answercount <= 0)
    {
      $('.input-error').show();
    }
    else
    {
      $('.input-error').hide();

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      });
      $.ajax({
          type: "POST",
          url: "{{route('users.answers.store')}}",
          data: $(activeform).serialize(),
          success: function(data) {
            if(data.status == "success")
            {
              $html = '<div class="answer-status-box pd-bt-20"><div class="card-header card-default bg-danger m-b-30"><h4 class="mt-0 mb-0 header-title text-white">Quiz '+activeformindex+'/3</h4></div><h4 class="mt-0 m-b-30 header-title light-red bold-font">'+data.question+'</h4>';
              if(data.answerstatus == 1)
              {
                $ansnot = '<div class="answer-status"><div class="alert alert-success" role="alert">'+data.answer+' <span><i class="fa fa-check" aria-hidden="true"></i></span></div></div>';
              }
              else
              {
                $ansnot = '<div class="answer-status"><div class="alert alert-danger" role="alert">'+data.answer+' <span><i class="fa fa-times" aria-hidden="true"></i></span></div></div>';
              }

              $html = $html+$ansnot+'</div>';
              
              $(activeform).removeClass('activeform');
              $(activeform).fadeOut(400, function() {
                  $(this).next('.queans-form').addClass('activeform');
                  if($(this).next('.btn-box').length > 0)
                  {
                      $(form).closest('.queans').find('.btn-box button').remove();
                  }
                  $(this).next().before($html);
              });
              $(form).closest('.queans').find('button.btn-next').removeAttr('disabled');
              $(form).closest('.queans').find('button.btn-submit').attr('disabled', true);

              if(numforms == activeformindex)
              {
                $.ajax({
                  type: "POST",
                  url: "{{route('users.result')}}",
                  data: { id : {{$module->id}}},
                  success: function(data) {
                    console.log(data);
                    var result = "";
                    result = '<div class="row">';
                    result += '<div class="col-lg-3 col-md-6"><div class="card mini-stat bg-primary user-detail-box"><div class="text-white"><h5 class="mb-3">Correct Answers</h5><h4 class="mb-2">'+data.correct_answer+'</h4></div></div></div>';
                    result += '<div class="col-lg-3 col-md-6"><div class="card mini-stat bg-primary user-detail-box"><div class="text-white"><h5 class="mb-3">Incorrect Answers</h5><h4 class="mb-2">'+data.incorrect_answer+'</h4></div></div></div>';
                    result += '<div class="col-lg-3 col-md-6"><div class="card mini-stat bg-primary user-detail-box"><div class="text-white"><h5 class="mb-3">Percentage</h5><h4 class="mb-2">'+data.percentage+'</h4></div></div></div>';
                    result += '<div class="col-lg-3 col-md-6"><div class="card mini-stat bg-primary user-detail-box"><div class="text-white"><h5 class="mb-3">Result</h5><h4 class="mb-2">'+data.result+'</h4></div></div></div>';
                    result += '</div>';

                    if((data.result).toLowerCase() == 'fail')
                    {
                      result += '<div class="button-items"><a class="btn btn-primary waves-effect waves-light" href="{{route('users.modules.show', ['id' => $module->id, 'retry' => 'retry'])}}" role="button">Try again</a></div>';
                    }

                    $('.queans').append(result);
                  }
                });
              }
            }
          },
          error: function(data){
              
          }
      });
    }
  }
</script>
@endsection