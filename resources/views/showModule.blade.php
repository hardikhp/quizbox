@extends('layouts.app')
@section('content')
<!-- Start content -->
<div class="content modules-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Module</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('modules')}}">Modules</a></li>
                        <li class="breadcrumb-item active">{{$module->name}}</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">{{$module->name}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="directory-content pd-lr-0 pd-tb-0">
                                    <h3 class="font-20">Course : {{$course->name}}</h3>
                                    <div class="progress m-b-10" style="height: 2px;">
                                        <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6 col-md-6">
                                <div class="card directory-card m-b-20">
                                    <div>
                                        <div class="directory-content p-4">
                                            <h5 class="mt-0 font-16">{{$module->name}}</h5>
                                            {!! ($module->description) !!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-xl-6 col-md-6">
                                <div class="card directory-card m-b-20">
                                    <div>
                                        <div class="directory-content text-center p-4">
                                            @if($module->video != "")
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <span class= "pip">
                                                    <video width="100%" height="240" controls controlsList="nodownload">  
                                                    <source src="{{ $module->video }}" type="video/mp4">  
                                                    Your browser does not support the video tag.
                                                    </video> 
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="directory-content pd-lr-0 pd-tb-0">
                                    <h3 class="font-20">Questions Answers</h3>
                                    <div class="progress m-b-10" style="height: 2px;">
                                        <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="card directory-card m-b-20">
                                    <div>
                                        @if(isset($module->questions) && $module->questions !== null)
                                            @foreach($module->questions as $question)
                                                @php $queindex = $loop->index @endphp
                                                <div class="directory-content p-4 pd-bt-20">
                                                    <h4 class="mt-0 header-title light-red bold-font">{{$question->question}}</h4>
                                                    @if(isset($question->answers) && $question->answers !== null)
                                                        @foreach($question->answers as $anskey => $ansofque)
                                                            <button type="button" class="btn text-left btn-block waves-effect waves-light {{ $ansofque->is_correct == 1 ? 'btn-primary' : '' }}">{{$ansofque->answer}}</button>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->
</div> <!-- content -->
@endsection