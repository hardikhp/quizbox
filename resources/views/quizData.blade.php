<style>
  body{font-family:'Nunito', sans-serif;font-size: 14px;line-height:16px;font-weight:500;color:#000;letter-spacing: 0.3px;}
  table { width: 100%; }
  span.btm { display: block; font-size: 11px; line-height: 13px; text-transform: capitalize;}
  table tr th.heading { font-weight: bold; border: 0; }
  table thead tr th { font-weight: normal; border-bottom: 1px solid #999; }
  .light-black { color: #222;}
  .alert-success { color: #2e6740; background-color: #d6f6e0; font-weight: 600;}
  .alert-danger { color: #ec536c; background-color: #fbdde2; font-weight: 600;}
  .alert-success span { background: rgba(43, 121, 69, 0.7);}
  .alert-danger span { background: rgba(236, 83, 108, 0.7);}
  .answer-status span { margin-left: 0; margin-right: 10px; width: 20px; height: 20px;}
  .answer-status .alert { padding: .5rem .5rem; margin-bottom: 0;}
  .user-detail-box { padding: 10px 15px; }
  .user-detail-box h5 { font-size: 14px; margin-bottom: 5px;min-height: 30px }
  .user-detail-box h4 { font-size: 20px; }
  .answer-status { margin-bottom: 20px;}
  h4 { margin-bottom: 7px; }
</style>
<div class="main-contain">
	<div class="row">
		<div class="col-sm-12" style="border: 0; background-color: #17a2b8; padding: 10px 20px 0px; margin-bottom: 20px;">
  			<h1 style="color: #fff;">QuizBox</h1>
    </div> 
  </div> 
  <div class="row">
  	<div class="col-12" style="margin-bottom: 0; color: #222;">
  		<h2>User Details:</h2>
  	</div>
  </div> 
  <div class="row" style="margin-bottom: 0;">  	
  	<div class="col-sm-12">
  		<div class="card mini-stat bg-primary user-detail-box">
              <div class="text-white">
                  <h5 class="text-uppercase mb-3">User Name</h5>
                  <h4 class="mb-4">{{$username}}</h4>
              </div>
          </div>
  	</div>
  </div> 
  <div class="row">
   	<div class="col-12" style="margin-bottom: 0; color: #222;">
   		<h2>Quiz Result:</h2>
   </div>
	</div>
  <div class="row" style="margin-bottom: 0;">
		<div class="col-lg-3 col-md-6">
 			<div class="card mini-stat bg-primary user-detail-box">
              <div class="text-white">
                  <h5 class="text-uppercase mb-3">Correct Answers</h5>
                  <h4 class="mb-4">{{$correct}}</h4>
              </div>
          </div>
 		</div>
 		<div class="col-lg-3 col-md-6">
		<div class="card mini-stat bg-primary user-detail-box">
              <div class="text-white">
                  <h5 class="text-uppercase mb-3">Incorrect Answers</h5>
                  <h4 class="mb-4">{{$incorrect}}</h4>
              </div>
          </div>
 		</div>
 		<div class="col-lg-3 col-md-6">
 			<div class="card mini-stat bg-primary user-detail-box">
              <div class="text-white">
                  <h5 class="text-uppercase mb-3">Percentage</h5>
                  <h4 class="mb-4">{{$percentage}} %</h4>
              </div>
          </div>
 		</div>
 		<div class="col-lg-3 col-md-6">
 			<div class="card mini-stat bg-primary user-detail-box">
              <div class="text-white">
                  <h5 class="text-uppercase mb-3">Result</h5>
                  <h4 class="mb-4">{{$results}}</h4>
              </div>
          </div>
 		</div>
  </div>  
  @if(isset($retrycount) && $retrycount > 0)
      <h4 class="mt-0 m-b-30 header-title bold-font tryquiz-count">Quiz Try: {{$retrycount}}</h4>
  @endif
 	@foreach($model_data as $model)
  	@if($model->is_correct==1)
	    	<h4 class="mt-0 m-b-10 header-title light-black bold-font">Q. {{ $model->question }}</h4>
	    	<div class="answer-status">
				<div class="alert alert-success" role="alert">
					<img src="{{asset('assets/images/check.png')}}" alt="user" class="rounded-circle" width="15" style="margin-top: 3px; margin-right: 5px;">
					A. {{ $model->answer }}
				</div>
			</div>
    @else
    		<h4 class="mt-0 m-b-10 header-title light-black bold-font">Q. {{ $model->question }}</h4>
	        <div class="answer-status">
				<div class="alert alert-danger" role="alert">
					<img src="{{asset('assets/images/wrong.png')}}" alt="user" class="rounded-circle" width="15" style="margin-top: 3px; margin-right: 5px;">
					A. {{ $model->answer }}
				</div>
			</div>
	  
	@endif
	@endforeach
</div>