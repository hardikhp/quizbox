<?php
namespace Modules\Admin\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile_picture'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the courses for the admin user.
     */
    public function courses()
    {
        return $this->hasMany('App\Models\Course', 'created_by');
    }

    /**
     * Get the companies for the admin user.
     */
    public function companies()
    {
        return $this->hasMany('App\Models\Company', 'created_by');
    }
}