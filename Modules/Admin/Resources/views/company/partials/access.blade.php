<form id="course-access" method="post" class="form-horizontal" action="">
    <div class="form-group">
        <label class="control-label">Courses</label>

        <select name="courses[]" class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Choose ...">

            <option value="">Select Course Name</option>
            @foreach($courses as $course)
                <option @if(in_array($course->id, $course_ids))selected="selected"@endif value="{{$course->id}}">{{$course->name}}</option>
            @endforeach
        </select>
        <input type="hidden" name="company_id" value="{{$company_id}}">
    </div>
    <div class="form-group">
    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
    </div>

</form>

<script type="text/javascript">
$('#course-access').on('submit',function(e){
    e.preventDefault(e);
    $("#AccessModal").modal('hide');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
    $.ajax({
        type:'POST',
        url:'{{route('admin.companies.access')}}',
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
        },
        error: function(data){

        }
    });
});
</script>