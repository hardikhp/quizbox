@extends('admin::layouts.master')
@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit Company</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.companies')}}">Companies</a></li>
                        <li class="breadcrumb-item active">Edit Company</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                @if (count($errors) > 0)
                <div class="row">
                    <div class="card-body text-white">
                        <div class="alert bg-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        @foreach ($errors->all() as $error)
                            <strong>Error !</strong> {{ $error }}<br/>
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <div class="card m-b-20">
                    <div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Edit Company</h4>
                    </div>
                    <div class="card-body">
                        <form id="AddCompanyForm" method="post" class="form-horizontal" action="{{ route('admin.companies.update') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" value="{{$company->name}}" id="name" name="name" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-2 col-form-label">Website</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" type="text" value="{{$company->website}}" id="website" name="website" required>
                                    @if ($errors->has('website'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('website') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <input type="hidden" name="id" value="{{$company->id}}">
                                <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')
@endsection