@extends('admin::layouts.master')
@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Edit User</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.companies')}}">Companies</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.companies.users', ['id' => $company->id])}}">Users</a></li>
                        <li class="breadcrumb-item active">Edit User</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                @if (count($errors) > 0)
                <div class="row">
                    <div class="card-body text-white">
                        <div class="alert bg-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        @foreach ($errors->all() as $error)
                            <strong>Error !</strong> {{ $error }}<br/>
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <div class="card m-b-20">
                    <div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Edit User</h4>
                    </div>
                    <div class="card-body">
                        <form id="AddCompanyForm" method="post" class="form-horizontal" action="{{ route('admin.companies.users.update') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" value="{{$user->name}}" id="name" name="name" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="company" class="col-sm-2 col-form-label">Company</label>
                                <div class="col-sm-10">
                                    <select class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" id="company" required>
                                        <option value="">Select Company</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}" @if($company->id == $user->company_id) selected @endif>{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('company'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('company') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Role</label>
                                <div class="col-sm-10">
                                    <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" id="role" required>
                                        <option value="">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @if(!empty($user->roles()->first()) && $role->id == $user->roles()->first()->id) selected @endif>{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" value="" id="password" name="password">
                                    <sub>Leave blank if don't want to change password.</sub>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="password" value="" id="password_confirmation" name="password_confirmation">
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <input type="hidden" name="id" value="{{$user->id}}">
                                <input type="hidden" name="company_id" value="{{$user->company_id}}">
                                <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')
@endsection