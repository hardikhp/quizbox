@extends('admin::layouts.master')
@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Company</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.companies')}}">Companies</a></li>
                        <li class="breadcrumb-item active">Add Company</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                @if (count($errors) > 0)
                <div class="row">
                    <div class="card-body text-white">
                        <div class="alert bg-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        @foreach ($errors->all() as $error)
                            <strong>Error !</strong> {{ $error }}<br/>
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <form id="AddCourseForm" method="post" class="form-horizontal" action="{{ route('admin.companies.store') }}" enctype="multipart/form-data">
                    <div class="card m-b-20">
                        <div class="card-header card-default">
                            <h4 class="mt-0 mb-0 header-title">Company Details</h4>
                        </div>
                        <div class="card-body">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" value="{{old('name')}}" id="name" name="name" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-2 col-form-label">Website</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" type="text" value="{{old('website')}}" id="website" name="website">
                                    @if ($errors->has('website'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('website') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card m-b-20">
                        <div class="card-header card-default">
                            <h4 class="mt-0 mb-0 header-title">Company's Default User Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="user_name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" type="text" value="{{old('user_name')}}" id="user_name" name="user_name" required>
                                    @if ($errors->has('user_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" value="{{old('email')}}" id="email" name="email" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" value="" id="password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="password" value="" id="password_confirmation" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!-- end col -->
        </div> <!-- end row -->


    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')

@endsection