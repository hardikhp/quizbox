@extends('admin::layouts.master')

@section('PageCSS')
<!-- DataTables -->
<link href="{{url('/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

    	<div class="row">
    		@if(Session::has('message'))
			    <div class="card-body">
			        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible" role="alert">
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                 <span aria-hidden="true">×</span>
			            </button>{{ Session::get('message') }}
			        </div>
			    </div>
			@endif
    	</div>

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Company {{$company->name}}: Users</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.companies')}}">Companies</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>

                    <div class="state-information d-none d-sm-block">
                        <a href="{{route('admin.companies.users.create', ['company_id' => $company->id])}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New User</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                	<div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Company {{$company->name}}: Users</h4>
                    </div>
                    <div class="card-body">

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->

</div> <!-- content -->
<div id="AccessModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="AccessModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="AccessModalLabel">Course Access</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="AccessModalLabel" aria-hidden="true">
    <form action="{{ route('admin.companies.users.delete') }}" method="POST" class="form-horizontal m-t-30">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="AccessModalLabel">Are you sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete this user?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="userid" value="">
                    <input type="hidden" name="companyid" id="companyid" value="">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.modal -->
@endsection

@section('PageJS')
<!-- Required datatable js -->
<script src="{{url('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<script src="{{url('/plugins/select2/js/select2.min.js')}}"></script>

<!-- Datatable init js -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{{ route('admin.companies.users.json', ['id' => $company->id]) }}',
	        columns: [
	            { data: 'id', name: 'id'},
	            { data: 'name', name: 'name' },
	            { data: 'email', name: 'email' },
                { data: 'role', name: 'role', orderable: false, searchable: false},
	            { data: 'created_at', name: 'created_at'},
	            { data: 'action', name: 'action', orderable: false, searchable: false },
	        ]
	    });
	});
    function openAccessModal(company_id, user_id)
    {
        $.ajax({
            url: "{{route('admin.users.access')}}",
            data: {company_id: company_id, user_id: user_id},
            success: function(res){
                $('#AccessModal .modal-body').html(res);
                $('#AccessModal').modal('show');
                $(".select2").select2();
            },
            error: function(err){
                alert('Something went wrong. Please reload your browser and try again.')
            }
        });
    }
    function confirmModal(company_id, user_id)
    {
        var user_id = user_id;
        var company_id = company_id;
        $('#userid').val(user_id);
        $('#companyid').val(company_id);
        $('#confirmModal').modal('show');
    }
</script>
@endsection