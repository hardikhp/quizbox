    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'QuizBox') }}</title>
        <meta content="Admin Dashboard - QuizBox" name="description" />
        <link rel="shortcut icon" href="{{ url('/assets/images/favicon.ico') }}">

        <link rel="stylesheet" href="{{ url('/plugins/morris/morris.css') }}">

        <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('/assets/css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('/assets/css/custom.css') }}" rel="stylesheet" type="text/css">

        @yield('PageCSS')
    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            @include('admin::layouts.partials.topBar')

            @include('admin::layouts.partials.leftBar')

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                @yield('content')

                
            </div>
            <footer class="footer">
                © {{date('Y')}} {{ config('app.name', 'QuizBox') }}, Hardik & Manan - All rights reserved </span>.
            </footer>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
            

        <!-- jQuery  -->
        <script src="{{ url('/assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('/assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ url('/assets/js/metisMenu.min.js') }}"></script>
        <script src="{{ url('/assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('/assets/js/waves.min.js') }}"></script>

        <script src="{{ url('/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

        <!--Morris Chart-->
        <script src="{{ url('/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ url('/plugins/raphael/raphael-min.js') }}"></script>
        <script src="{{ url('/assets/pages/dashboard.js') }}"></script>

        <!-- App js -->
        <script src="{{ url('/assets/js/app.js') }}"></script>
        @yield('PageJS')
</body>

</html>