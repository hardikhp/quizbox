@extends('admin::layouts.master')
@section('content')
<!-- Start content -->
<div id='loader' style="display: none;"><img src="{{ asset('assets/images/loader.gif') }}"></div>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Module</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.modules')}}">Modules</a></li>
                        <li class="breadcrumb-item active">Add Module</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                @if (count($errors) > 0)
                <div class="row">
                    <div class="card-body text-white">
                        <div class="alert bg-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        @foreach ($errors->all() as $error)
                            <strong>Error !</strong> {{ $error }}<br/>
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <div class="row module-errors">
                    <div class="card-body text-white">
                        <div class="alert bg-danger alert-dismissible" id="module-errors" role="alert" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card m-b-20">
                    <div class="card-header card-default">
                        <h4 class="mt-0 mb-0 header-title">Add Module</h4>
                    </div>
                    <div class="card-body">
                        <form id="AddModuleForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="course_id" class="col-sm-2 col-form-label">Course</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="course_id" id="course_id">
                                        <option value="">Select Course Name</option>
                                        @foreach($courses as $course)
                                            <option @if($course->id == old('course_id')) selected="selected"@endif value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" value="{{old('name')}}" id="name" name="name" autocomplete="off" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" id="description" name="description">{{old('description')}}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="meta_keywords" class="col-sm-2 col-form-label">Meta Keywords</label>
                                <div class="col-sm-10">
                                    <input class="form-control{{ $errors->has('meta_keywords') ? ' is-invalid' : '' }}" type="text" value="{{old('meta_keywords')}}" id="meta_keywords" name="meta_keywords">
                                    @if ($errors->has('meta_keywords'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('meta_keywords') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="meta_description" class="col-sm-2 col-form-label">Meta Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control{{ $errors->has('meta_description') ? ' is-invalid' : '' }}" id="meta_description" name="meta_description">{{old('meta_description')}}</textarea>
                                    @if ($errors->has('meta_description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('meta_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="meta_description" class="col-sm-2 col-form-label">Pass Percentage</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="percentage" min="0.00" max="100.00" id="percentage">
                                    @if ($errors->has('percentage'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('percentage') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row choose_upload_method">
                                <label for="meta_description" class="col-sm-2 col-form-label">Select Upload Method:</label>
                                <div class="col-sm-10">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="radio" name="upload_method" id="option1" value="upload" checked> Upload Video
                                        </label>
                                       {{--  <label class="btn btn-primary">
                                            <input type="radio" name="upload_method" id="option2" value="capture"> Capture Video
                                        </label> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="upload_video">
                                <label for="meta_description" class="col-sm-2 col-form-label">Upload Video</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="video" accept=".mp4,.mpeg,.webm" id="video_up">
                                </div>
                            </div>
                            <div class="form-group row" id="capture_video">
                                <label for="meta_description" class="col-sm-2 col-form-label">Capture Video</label>
                                <div class="col-sm-10">
                                    <button id="btn-start-recording" type="button" class="btn btn-info btn-icon margin-r-5 mr-bt-10" style="display:block;">Start Recording</button>
                                    <button id="btn-pause_hp-recording" type="button"  class="btn btn-primary btn-icon margin-r-5 mr-bt-10" style="display:none;">Pause</button>
                                    <button id="btn-play_hp-recording" type="button"  class="btn btn-primary btn-icon margin-r-5 mr-bt-10" style="display:none;">Play</button>
                                    <button id="btn-stop-recording" type="button"  class="btn btn-info btn-icon margin-r-5 mr-bt-10" style="display:none;">Stop Recording</button>
                                    
                                <video autoplay id="hpvideo" style="display: none"></video>
                                    <div>
                                      <label id="timer" style="display:none;font-size:24px;color:#6A5DB6"></label>  
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="ERROR_MESSAGE" style="display: none">
                                <label for="Error Message" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <span style="color: red;font-size: 16px">Media device not found. Please attach and try again!!</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="meta_description" class="col-sm-2 col-form-label">Questions</label>
                                <div class="col-sm-10 queans_container">
                                    <div class="queans-box">
                                        @if(old('questions') !== null)
                                            @foreach(old('questions') as $question)
                                                @php $queindex = $loop->index @endphp
                                                <div class="form-group queans-boxinner">
                                                    <div class="row questions-row">
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" name="questions[]" placeholder="Question">{{$question}}</textarea>
                                                        </div>
                                                        <div class="col-sm-2 remove_queans_field">
                                                            <button type="button" class="btn btn-sm btn-danger remove_field" onclick="removeQuestion(this);">Remove</button>
                                                        </div>
                                                    </div>
                                                    <div class="answers-box">
                                                        @if(old('answers') !== null)
                                                            @foreach(old('answers') as $ansofque)
                                                                @if($queindex == $loop->index)
                                                                    @foreach($ansofque as $answer)
                                                                        @php $ansindex = $loop->index @endphp
                                                                        <div class="row answers-row">
                                                                            <div class="col-sm-8">
                                                                                <textarea class="form-control" name="answers[{{$queindex}}][]" placeholder="Answer">{{$answer}}</textarea>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="btn-group" data-toggle="buttons">
                                                                                    <label class="btn btn-outline-primary waves-effect waves-light radansbox {{ (old('is_correct.'.$queindex) == ($ansindex + 1)) ? 'checked active' : '' }}" onclick="selectedAnswer(this);"><input type="radio" name="is_correct[{{$queindex}}]" value="{{$ansindex + 1}}" {{ (old('is_correct.'.$queindex) == ($ansindex + 1)) ? 'checked' : '' }}> Select Answer</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2 remove_queans_field">
                                                                                <button type="button" class="btn btn-sm btn-danger remove_ans" onclick="removeAnswer(this);">Remove</button>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                    <button type="button" class="btn btn-sm btn-primary add_more_ans" onclick="addAnswer(this, {{$queindex}});">Add Answer</button>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="form-group queans-boxinner">
                                                <div class="row questions-row">
                                                    <div class="col-sm-10">
                                                        <textarea class="form-control" name="questions[]" placeholder="Question"></textarea>
                                                    </div>
                                                    <div class="col-sm-2 remove_queans_field">
                                                        <button type="button" class="btn btn-sm btn-danger remove_field" onclick="removeQuestion(this);">Remove</button>
                                                    </div>
                                                </div>
                                                <div class="answers-box">
                                                </div>
                                                <button type="button" class="btn btn-sm btn-primary add_more_ans" onclick="addAnswer(this, 0);">Add Answer</button>
                                            </div>
                                        @endif
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary add_more_que" onclick="addQuestion(this);">Add More Questions</button>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary" name="Submit" value="Submit" id="add_new">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('PageJS')

<script src="{{ url('/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    $(document).ready(function () {
        if($("#description").length > 0){
            tinymce.init({
                selector: "textarea#description",
                browser_spellcheck: true,
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
        $("#AddModuleForm").submit(function(e){
            e.preventDefault();
            video_cap();
        });
        $("#capture_video").hide();
        $(".choose_upload_method input[type=radio]").change(function() {
            if (this.value == 'upload') {
                $("#capture_video").hide();
                $("#upload_video").show();
                $("#add_new").show();
                $("#ERROR_MESSAGE").hide();
            }
            else if (this.value == 'capture') {
                $("#capture_video").show();
                $("#upload_video").hide();
                $("#add_new").hide();
            }
        });
    });
    var x = 0;
    function addQuestion(currentQuestionBox){
        x++;
        $('.queans-box').append('<div class="form-group queans-boxinner"> <div class="row questions-row"> <div class="col-sm-10"> <textarea class="form-control" name="questions[]" placeholder="Question"></textarea> </div> <div class="col-sm-2 remove_queans_field"> <button type="button" class="btn btn-sm btn-danger remove_field" onclick="removeQuestion(this);">Remove</button> </div> </div> <div class="answers-box"> </div> <button type="button" class="btn btn-sm btn-primary add_more_ans" onclick="addAnswer(this, '+x+');">Add Answer</button> </div>');
    }
    function removeQuestion(currentQuestion){
        $(currentQuestion).parent().parent().parent('.queans-boxinner').remove();
    }
    function addAnswer(currentQuestionBox, quesnum){
        var ansval = $(currentQuestionBox).closest('.queans-boxinner').find('.answers-row').length + 1;
        $(currentQuestionBox).closest('.queans-boxinner').find('.answers-box').append('<div class="row answers-row"><div class="col-sm-8"> <textarea class="form-control" name="answers['+quesnum+'][]" placeholder="Answer"></textarea> </div> <div class="col-sm-2"> <div class="btn-group" data-toggle="buttons"> <label class="btn btn-outline-primary waves-effect waves-light radansbox" onclick="selectedAnswer(this);"> <input type="radio" name="is_correct['+quesnum+']" value="'+ansval+'"> Select Answer </label> </div> </div> <div class="col-sm-2 remove_queans_field"> <button type="button" class="btn btn-sm btn-danger remove_ans" onclick="removeAnswer(this);">Remove</button> </div> </div>');
    }
    function removeAnswer(currentAnswerBox){
        $(currentAnswerBox).parent().parent('.answers-row').remove();
    }
    function selectedAnswer(selectedAnswerInput){
        $(selectedAnswerInput).closest('.answers-box').find('.radansbox').removeClass('checked');
        $(selectedAnswerInput).addClass('checked');
    }
</script>

<script src="{{asset('js/RecordRTC.js')}}"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>

<script type="text/javascript">
   
</script>
<script>

    var video = document.getElementById('hpvideo');
    var recorder="";
    var blob="";
    var mm="00",mm1="00";
    var hh="00",hh1="00";
    var ss="00",ss1="00";
    var ti=document.getElementById("timer");
    var isPaused = false;
   
    document.getElementById('btn-start-recording').addEventListener("click", function(){

     video.style.display="block";    
    document.getElementById("ERROR_MESSAGE").style.display="none";
    navigator.mediaDevices.getUserMedia({
        audio: true, 
        video: true
    }).then(function(stream) {
        setSrcObject(stream, video);
        video.play();
        video.muted = true;
        
        recorder = new RecordRTCPromisesHandler(stream, {
            mimeType: 'video/webm',
            bitsPerSecond: 128000
        });
        
        recorder.startRecording().then(function() {
            console.info('Recording video ...');
        }).catch(function(error) {
            console.error('Cannot start video recording: ', error);
        });
        recorder.stream = stream;
        document.getElementById('btn-pause_hp-recording').style.display ="inline-block";
        document.getElementById('btn-play_hp-recording').style.display ="inline-block";
        document.getElementById('btn-stop-recording').style.display ="inline-block";
        document.getElementById('btn-start-recording').style.display ="none";
        ti.style.display="inline-block";
        setInterval(function() {
          if(!isPaused){
         if (ss <= 60) {
             ss++;
             if(ss<10){
               ss="0"+ss;
             }
           }
           if(ss==60){
             mm++;
             if(mm<10){
               mm="0"+mm;
             }
             ss=0;
           }
           if(mm==60){
             hh+=1;
             mm=0;
             if(hh<10){
               hh="0"+hh;
             }
           }
           var t=hh + ":" + mm + ":" + ss;
           ti.innerHTML=t;
         }
       }, 1000);
    }).catch(function(error) {
        video.style.display="none";
        document.getElementById("ERROR_MESSAGE").style.display="flex";
    });
}, false);

// When the user clicks on Stop video recording
document.getElementById('btn-stop-recording').addEventListener("click", function(e){
       

    document.getElementById("ERROR_MESSAGE").style.display="none";
    recorder.stopRecording().then(function() {
        e.preventDefault();
        isPaused = true;
        ti.style.display="none";
        hh1=hh;ss1=ss;mm1=mm;
        var t=hh1 + ":" + mm1 + ":" + ss1;
        ti.innerHTML=t;
        console.info('stopRecording success');
        blob = recorder.blob;
        video.muted = false;
        recorder.stream.stop();
        document.getElementById("hpvideo").style.display="none";
        document.getElementById("add_new").style.display="inline-block";
        document.getElementById('btn-stop-recording').style.display ="none";
        document.getElementById('btn-start-recording').style.display ="inline-block";
        document.getElementById('btn-pause_hp-recording').style.display ="none";
        document.getElementById('btn-play_hp-recording').style.display ="none";
    }).catch(function(error) {
        console.error('stopRecording failure', error);
    });
}, false);
 

document.getElementById('btn-play_hp-recording').addEventListener("click", function(e){
    e.preventDefault();
    isPaused = false;
    hh1=hh;ss1=ss;mm1=mm;
    var t=hh + ":" + mm + ":" + ss;
    ti.innerHTML=t;
    ti.style.display="block";
    ti.style.color="#6A5DB6";
    recorder.resumeRecording();
    document.getElementById("btn-play_hp-recording").style.display="none";
    document.getElementById("btn-pause_hp-recording").style.display="inline-block";
    document.getElementById("hpvideo").play();
   });
  document.getElementById('btn-pause_hp-recording').addEventListener("click", function(e){
    ti.style.display="block";
    e.preventDefault();
    isPaused = true;
    hh1=hh;ss1=ss;mm1=mm;
    var t=hh1 + ":" + mm1 + ":" + ss1;
    ti.innerHTML=t;
    ti.style.color="#EC536B";
    recorder.pauseRecording();
    document.getElementById("hpvideo").pause();
    document.getElementById("btn-play_hp-recording").style.display="inline-block";
    document.getElementById("btn-pause_hp-recording").style.display="none";
  });

function video_cap(){
    
    var formData = new FormData(document.getElementById("AddModuleForm"));
    var video=jQuery('#video_up').val();
    if(!video && !blob)
    {
        return alert('No video found.');
    }
    formData.append("fileType",blob);
    formData.append("_token", "{{ csrf_token() }}");
    var upload_url = "{{ route('admin.modules.store') }}";
    var upload_directory = "{{ asset('/uploads/videos/') }}"+'/';
    makeXMLHttpRequest(upload_url, formData, function(progress) {
        var moduleres;
        var uploadend;
        if(progress.uploadend != "")
        {
            uploadend = progress.uploadend;
        }
        if(progress.data != "")
        {
            if(typeof(progress.data) === 'string')
            {
                moduleres = JSON.parse(progress.data);
            }
            else
            {
                moduleres = progress.data;
            }
            if(typeof(moduleres) !== 'undefined')
            {
                if(moduleres.status == "error")
                {
                    document.getElementById("loader").style.display="none";
                    var alertMsg = '';
                    $.each( moduleres.messages, function( key, message ) {
                        alertMsg += '<strong>'+ message + '</strong><br>';
                    });

                    errorlist = document.getElementById("errorlist");
                    if (errorlist !== null) {
                        errorlist.remove();
                    }

                    errorcontainer = document.getElementById("module-errors");
                    var list = document.createElement("div");
                    list.setAttribute("id", "errorlist");
                    list.innerHTML = alertMsg;
                    if (errorcontainer !== null) {
                        document.getElementById("module-errors").style.display="block";
                        errorcontainer.appendChild(list);
                    }
                    else
                    {
                        document.getElementById("module-errors").style.display="none";
                    }
                    return false;
                }
            }
        }
        else
        {
            if (uploadend === 'upload-ended')
            {
                
                document.getElementById("loader").style.display="none";
                window.location.href = "{{ route('admin.modules') }}";
            }
            else
            {
                if(progress.uploadfailed === 'progress-failed')
                {
                    document.getElementById("loader").style.display="none";
                    alert('Video failed to upload to server');
                }
            }
        }
    });
 }
 function makeXMLHttpRequest(url, data, callback) {
        document.getElementById("loader").style.display="block";
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback({data:this.responseText, uploadend:'upload-ended'});
            }
        };
        request.upload.onload = function() {
            callback({progressend:'progress-ended'});
        };
        request.upload.onerror = function(error) {
            callback({uploadfailed:'progress-failed'});
        };
        request.upload.onabort = function(error) {
            callback({uploadaborted:'Upload aborted.'});
        };
        request.open('POST', url);
        request.send(data);
    }
</script>     

@endsection