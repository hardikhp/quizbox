<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Course;
use App\Models\Video;
use App\Models\Question;
use App\Models\Answer;
use App\Models\User;
use App\Models\UserAnswer;
use App\Models\Company;
use App\Rules\QuestionsAnswers;
use App\Rules\SelectAnyAnswer;
use Modules\Admin\Models\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use PDF;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::module.index');
    }

    /**
     * JSON object of the resource.
     * @return Response
     */
    public function indexJson()
    {
        $modules = Video::query();
        return \DataTables::of($modules)
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->filterColumn('course_id', function($query, $keyword) {
                    $admins = Course::whereRaw("name LIKE ?", ["%{$keyword}%"])->select('id')->get();
                    $admin_user_ids = [];
                    foreach ($admins as $admin) {
                        $admin_user_ids[] = $admin->id;
                    }
                    if(!empty($admin_user_ids))
                        $query->whereIn('course_id', $admin_user_ids);
                })
                ->editColumn('course_id', function($data) {
                    return $data->video_course->name;
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $html = '<a href="'. route('admin.modules.edit', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a><a href="javascript::void(0);" onclick="confirmModal('. $data->id .')" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</a><a href="'. route('admin.modules.show', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-info"></i>User</a>';
                    return $html;
                })
                ->rawColumns(['course_id', 'created_at', 'action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {   
        $courses = Course::get();
        return view('admin::module.create')->with('courses',$courses);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'course_id' => 'required',
            'percentage' =>'required',
            'questions' => 'required|array',
            'questions.*' => 'required',
            'answers' => ['required','array',new QuestionsAnswers($data)],
            'answers.*.*'  => 'required',
            'is_correct' => ['required','array',new SelectAnyAnswer($data)],
            'video' => 'max:100000',
        ],[
              'course_id.required' => 'The course field is required',
              'questions.*.required' => 'The question field is required',
              'answers.*.*.required' => 'The answer field is required',
              'is_correct.required' => 'The select answer field is required',
              'percentage.required' =>'The percentage field is required',
        ]);

        if($validator->fails()) {
            //return redirect()->back()->withErrors($validator)->withInput();
            return response()->json([
                'type' => 'message',
                'status' => 'error',
                'messages' => $validator->getMessageBag()->toArray()
            ]);
        }

        try {
            // produce a slug based on the activity title
            $slug = Str::slug($request->get('name'));

            // check to see if any other slugs exist that are the same & count them
            $count = Video::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

            // if other slugs exist that are the same, append the count to the slug
            $slug = $count ? "{$slug}-{$count}" : $slug;

          
            $file = $request->file('video');
            if (isset($file)) 
            {
                $target_dir=public_path('uploads/videos');
                if(!file_exists($target_dir)){
                        mkdir($target_dir, 0777, true);
                }    
                $videofile = $file;
                $filename = time() . '_' . pathinfo($videofile->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $videofile->getClientOriginalExtension();
                $imageNameMULTI = array(
                    'name' => pathinfo($videofile->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $videofile->getClientOriginalExtension(),
                    'size' => $videofile->getSize(),
                    'path' => '/uploads/videos/'.$filename,
                    'file_name' => $filename,
                );
                $request->videofile = $videofile;
                $request->videofile->move(public_path('uploads/videos'), $filename);
                $video_up=$imageNameMULTI['path'];
            }else{
                   if (isset($_FILES["fileType"])) {
                    $fileName=mt_rand(1, 1000);
                    $fileName=$fileName.".webm";
                    $target_dir=public_path('uploads/videos');
                     if(!file_exists($target_dir)){
                            mkdir($target_dir, 0777, true);
                        }    
                    $uploadDirectory =  public_path('uploads/videos/').$fileName;
                    move_uploaded_file($_FILES["fileType"]["tmp_name"], $uploadDirectory);
                   }
               
                 $video_up='/uploads/videos/'.$fileName;
            }
           if(isset($video_up) && $video_up != ""){
                $module = Video::create([
                    'course_id' => $request->get('course_id'),
                    'name' => $request->get('name'),
                    'slug' => $slug,
                    'description' => $request->get('description'),
                    'meta_keywords' => $request->get('meta_keywords'),
                    'meta_description' => $request->get('meta_description'),
                    'video' => $video_up,
                    'pass_percentage' =>$request->get('percentage'),
                    'upload_method' => $request->get('upload_method'),
                ]);
                $lastVideoId = $module->id;

                if(isset($data['questions']) && !empty($data['questions']))
                {
                    foreach ($data['questions'] as $quekey => $value) {
                        $question = Question::create([
                            'video_id' => $lastVideoId,
                            'question' => $value,
                        ]);
                        $lastQuestionId = $question->id;

                        if(isset($data['answers']) && !empty($data['answers']) && !empty($data['answers'][$quekey]) && isset($data['is_correct'][$quekey]))
                        {
                            $correct_ans = $data['is_correct'][$quekey];
                            $ans_count = 1;
                            foreach ($data['answers'][$quekey] as $answer) {
                                $is_correct = 0;
                                if($ans_count == $correct_ans)
                                {
                                    $is_correct = 1;
                                }
                                $ans_count++;
                                $answercreated = Answer::create([
                                    'question_id' => $lastQuestionId,
                                    'answer' => $answer,
                                    'is_correct' => $is_correct
                                ]);
                                $lastAnswerId[] = $answercreated->id;
                            }
                        }
                    }
                }
            }            

            \Session::flash('message', 'Module added successfully.');
            \Session::flash('alert-class', 'bg-success');
           // return redirect(route('admin.modules'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            //return redirect(route('admin.modules'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $module = Video::findOrFail($id);
        $courses = Course::get();
        return view('admin::module.edit')->with('module',$module)->with('courses',$courses);
    }
   
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
       
       
        $validate_array = [
            'name' => ['required', 'max:255'],
            'course_id' => ['required'],
            'percentage' =>'required',
            'questions' => ['required','array'],
            'questions.*' => ['required'],
            'answers' => ['required','array',new QuestionsAnswers($data)],
            'answers.*.*'  => ['required'],
            'is_correct' => ['required','array',new SelectAnyAnswer($data)],
        ];

        if(!empty($request->file('video')))
        {
            $validate_array['video'] = ['required', 'mimes:mpeg,mp4,webm,qt'];
        }

        $validator = Validator::make($request->all(), $validate_array,[
              'course_id.required' => 'The course field is required',
              'questions.*.required' => 'The question field is required',
              'answers.*.*.required' => 'The answer field is required',
              'is_correct.required' => 'The select answer field is required',
              'percentage.required' =>'The percentage field is required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $module = Video::findOrFail($request->get('id'));
        try {
            // produce a slug based on the activity title
            $slug = Str::slug($request->get('name'));

            if($slug != $module->slug)
            {
                // check to see if any other slugs exist that are the same & count them
                $count = Video::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $slug = $count ? "{$slug}-{$count}" : $slug;
            }

            $modulevideo = Video::updateOrCreate([
                'id' => $module->id
            ], [
                'course_id' => $request->get('course_id'),
                'name' => $request->get('name'),
                'slug' => $slug,
                'description' => $request->get('description'),
                'meta_keywords' => $request->get('meta_keywords'),
                'meta_description' => $request->get('meta_description'),
                'pass_percentage' =>$request->get('percentage'),
                'upload_method' => $request->get('upload_method'),
            ]);
            $questions = $module->questions;
            foreach ($questions as $qkey => $question) {
                if(!in_array($question->id, $data['queid']))
                {
                    $question->delete();
                }

                foreach ($question->answers as $answer) {
                    if(!in_array($answer->id, $data['ansid'][$qkey]))
                    {
                        $answer->delete();
                    }
                }
            }

            foreach ($data['queid'] as $qkey => $qid) {
                $question = Question::updateOrCreate([
                    'id' => $qid
                ], [
                    'question' => $data['questions'][$qkey],
                    'video_id' => $module->id,
                ]);

                $correct_ans = $data['is_correct'][$qkey];
                $ans_count = 1;

                foreach ($data['ansid'][$qkey] as $akey => $aid) {

                    $is_correct = 0;
                    if($ans_count == $correct_ans)
                    {
                        $is_correct = 1;
                    }
                    $ans_count++;

                    Answer::updateOrCreate([
                        'id' => $aid
                    ], [
                        'answer' => $data['answers'][$qkey][$akey],
                        'question_id' => $question->id,
                        'is_correct' => $is_correct
                    ]);
                }
            }

            $videofile = $request->file('video');
            if (isset($videofile))
            {
                $filename = time() . '_' . pathinfo($videofile->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $videofile->getClientOriginalExtension();
                $imageNameMULTI = array(
                    'name' => pathinfo($videofile->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $videofile->getClientOriginalExtension(),
                    'size' => $videofile->getSize(),
                    'path' => '/uploads/videos/'.$filename,
                    'file_name' => $filename,
                );
                $video_path = public_path().$module->video;
                unlink($video_path);
                $request->videofile = $videofile;
                $request->videofile->move(public_path('uploads/videos'), $filename);
                
                $module->update([
                    "video" => $imageNameMULTI['path'],
                ]);
            }
            $videofile1=$request['fileType'];
            if(isset($videofile1))
            {
                   if (isset($_FILES["fileType"])) {
                    $fileName=mt_rand(1, 1000);
                    $fileName=$fileName.".webm";
                    $target_dir=public_path('uploads/videos');
                     if(!file_exists($target_dir)){
                            mkdir($target_dir, 0777, true);
                        }    
                    $uploadDirectory =  public_path('uploads/videos/').$fileName;
                    move_uploaded_file($_FILES["fileType"]["tmp_name"], $uploadDirectory);
                   }
               
                 $video_up='/uploads/videos/'.$fileName;
                  $module->update([
                    "video" => $video_up,
                ]);
            }


            \Session::flash('message', 'Module updated successfully.');
            \Session::flash('alert-class', 'bg-success');
           // return redirect(route('admin.modules'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
           //return redirect(route('admin.modules'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $module = Video::findOrFail($request->get('id'));
        try {
            $module->delete();
            \Session::flash('message', 'Module deleted successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.modules'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.modules'));
        }
    }
    public function capture(){
        return view('admin::module.capture');
    }

    public function results($id)
    {
        return view('admin::module.results')->with('id',$id);
    }
    
    public function resultsJson($id,Request $request)
    {
        $video_id = $id;
        $user_id = $request->userid;
        $retry_count = 0;
        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if(isset($user_answers['retry_count']) && $user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id');

      

        return \DataTables::of($user_results)
                ->filterColumn('user_id', function($query, $keyword) {
                })
                ->editColumn('Action',function($data){
                    $html = '<button type="button" class="btn btn-info btn-icon mr-rt-5" onclick="modelOpen('.$data->user_id.');" ><i class="fa fa-info"></i> Result</button>';
                    $html .= '<a href="'. route('admin.exportpdf', ['userid' => $data->user_id, 'video_id' => $data->video_id]) .'" class="btn btn-info btn-icon mr-rt-5"><i class="fa fa-download"></i> PDF</a>';
                    return $html;
                })
                ->editColumn('user_id', function($data) {
                    $user = User::find($data->user_id);
                    return $user['name'];
                })
                ->editColumn('company', function($data) {
                    $user = User::find($data->user_id);
                    $company = Company::find($user['company_id']);
                    return $company['name'];
                })
                ->editColumn('Incorrect',function($data){
                    $in=$data['Total']-$data['correct'];
                    return $in;
                })
                ->editColumn('Percentage',function($data){
                    
                    $per=($data['correct']/$data['Total'])*100;
                    $per=round($per,2);
                    $per=$per;
                    return $per;
                })
                ->editColumn('Result',function($data){
                    $video_data=Video::find($data->video_id);
                    $percentage=$video_data['pass_percentage'];

                    $per=($data['correct']/$data['Total'])*100;
                    $per=round($per,2);
                    if($per>=$percentage){
                        return "<span style='color:green'>Pass</span>";
                    }else{
                        return "<span style='color:Red'>Fail</span>";
                    }
                })

                ->rawColumns(['user_id', 'company', 'Result', 'Action'])
                ->make(true);
    }

    
    public function modelData(Request $request){
        $video_id = $request->video_id;
        $user_id = $request->userid;
        $user = User::find($user_id);
        $username = $user['name'];
        $company = Company::find($user['company_id']);
        $companyname = $company['name'];
        $retry_count = 0;


        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($user_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id')->get();

        foreach ($user_results as $key => $value) {
            $correct = $value['correct'];
            $total = $value['Total'];
            $incorrect = $total - $correct;
            $percentage = ($correct/$total) * 100;

            $video_data = Video::find($video_id);
            $pass_percentage = $video_data['pass_percentage'];
            $percentage = round($percentage,2);

            if($percentage >= $pass_percentage){
                $results = "Pass";
            }else{
                $results = "Fail";
            }
        }

        $model_data = \DB::table('user_answers as ua')
                    ->select('ua.question','ua.answer','ua.is_correct')
                    ->join('questions as q','q.id','=','ua.question_id')
                    ->where('q.video_id',$video_id)
                    ->where('ua.user_id',$user_id)
                    ->where('ua.retry_quiz', '=', $retry_count)
                    ->get();
        
        $view = view('admin::module.quizData',[
            'model_data' => $model_data,
            'correct' => $correct,
            'incorrect' => $incorrect,
            'percentage' => $percentage,
            'results' => $results,
            'retrycount' => $retry_count,
            'username' => $username,
            'companyname' => $companyname,
        ]);
        echo $view->render();
        

    }


    public function exportPDF(Request $request)
    {
        $video_id = $request->video_id;
        $user_id = $request->userid;
        $user = User::find($user_id);
        $username = $user['name'];
        $company = Company::find($user['company_id']);
        $companyname = $company['name'];
        $retry_count = 0;

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($user_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id')->get();

        foreach ($user_results as $key => $value) {
            $correct = $value['correct'];
            $total = $value['Total'];
            $incorrect = $total - $correct;
            $percentage = ($correct/$total) * 100;

            $video_data = Video::find($video_id);
            $pass_percentage = $video_data['pass_percentage'];
            $percentage = round($percentage,2);

            if($percentage >= $pass_percentage){
                $results = "Pass";
            }else{
                $results = "Fail";
            }
        }

        $model_data = \DB::table('user_answers as ua')
                    ->select('ua.question','ua.answer','ua.is_correct')
                    ->join('questions as q','q.id','=','ua.question_id')
                    ->where('q.video_id',$video_id)
                    ->where('ua.user_id',$user_id)
                    ->where('ua.retry_quiz', '=', $retry_count)
                    ->get();

        $pdf = PDF::loadView('admin::exportpdf', compact('model_data','correct','incorrect','percentage','results','username','companyname','retry_count'));
        $pdf->save(storage_path().'_filename.pdf');
        //return $pdf->stream();
        return $pdf->download($user->name.'_'.$user_id.'.pdf');
        //return view('taxcalculations.exportpdf');
    }
}
