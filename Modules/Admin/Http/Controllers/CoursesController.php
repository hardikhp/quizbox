<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Course;
use Modules\Admin\Models\Admin;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::course.index');
    }

    /**
     * JSON object of the resource.
     * @return Response
     */
    public function indexJson()
    {
        $courses = Course::query();
        return \DataTables::of($courses)
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->filterColumn('created_by', function($query, $keyword) {
                    $admins = Admin::whereRaw("name LIKE ?", ["%{$keyword}%"])->select('id')->get();
                    $admin_user_ids = [];
                    foreach ($admins as $admin) {
                        $admin_user_ids[] = $admin->id;
                    }
                    if(!empty($admin_user_ids))
                        $query->whereIn('created_by', $admin_user_ids);
                })
                ->editColumn('created_by', function($data) {
                    return $data->created_user->name;
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $html = '<a href="'. route('admin.courses.edit', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a><a href="javascript::void(0);" onclick="confirmModal('. $data->id .')" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</a>';
                    return $html;
                })
                ->rawColumns(['created_by', 'created_at', 'action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::course.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);

        try {
            // produce a slug based on the activity title
            $slug = Str::slug($request->get('name'));

            // check to see if any other slugs exist that are the same & count them
            $count = Course::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

            // if other slugs exist that are the same, append the count to the slug
            $slug = $count ? "{$slug}-{$count}" : $slug;

            $course = Course::create([
                'created_by' => \Auth::guard('admin')->user()->id,
                'name' => $request->get('name'),
                'slug' => $slug,
                'description' => $request->get('description'),
                'meta_keywords' => $request->get('meta_keywords'),
                'meta_description' => $request->get('meta_description'),
            ]);
            \Session::flash('message', 'Course added successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.courses'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.courses'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        return view('admin::course.edit')->with('course', $course);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);
        $course = Course::findOrFail($request->get('id'));
        try {
            // produce a slug based on the activity title
            $slug = Str::slug($request->get('name'));

            if($slug != $course->slug)
            {
                // check to see if any other slugs exist that are the same & count them
                $count = Course::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

                // if other slugs exist that are the same, append the count to the slug
                $slug = $count ? "{$slug}-{$count}" : $slug;
            }

            Course::updateOrCreate([
                'id' => $course->id
            ], [
                'name' => $request->get('name'),
                'slug' => $slug,
                'description' => $request->get('description'),
                'meta_keywords' => $request->get('meta_keywords'),
                'meta_description' => $request->get('meta_description'),
            ]);
            \Session::flash('message', 'Course updated successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.courses'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.courses'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $course = Course::findOrFail($request->get('id'));
        try {
            $course->delete();
            \Session::flash('message', 'Course deleted successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.courses'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.courses'));
        }
    }
}
