<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Company;
use App\Models\Course;
use App\Models\CoursePermission;
use App\Models\User;
use App\Models\Role;
use Modules\Admin\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Emails\CompanyCreateEmailForDefaultUser;
use Modules\Admin\Emails\UserPasswordChangeByAdmin;
use Modules\Admin\Emails\CourseAccess;
use Illuminate\Support\Facades\Validator;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::company.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function users($id)
    {
        $company = Company::findOrFail($id);
        return view('admin::company.users')->with('company', $company);
    }

    /**
     * JSON object of the resource.
     * @return Response
     */
    public function usersJson($id)
    {
        $company = Company::findOrFail($id);
        return \DataTables::of($company->users())
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->editColumn('role', function($data) {
                    $roles = $data->roles()->get();
                    $roles_array = [];
                    foreach ($roles as $role) {
                        $roles_array[] = $role->display_name;
                    }
                    return implode(', ', $roles_array);
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $user = '';
                    $roles = $data->roles()->get();
                    foreach ($roles as $role) {
                        if($role->name == "user")
                        {    
                            $user = '<a href="javascript:void(0);" onclick="openAccessModal('. $data->company_id .','. $data->id .')" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-key"></i>Access</a>';
                        }
                    }

                    $html = '<a href="'. route('admin.companies.users.edit', ['company_id' => $data->company_id, 'id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a>'.$user.'<a href="javascript::void(0);" onclick="confirmModal('. $data->company_id .', '. $data->id .')" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</a>';
                    return $html;
                })
                ->rawColumns(['role', 'created_at', 'action'])
                ->make(true);
    }

    /**
     * JSON object of the resource.
     * @return Response
     */
    public function indexJson()
    {
        $companies = Company::query();
        return \DataTables::of($companies)
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->filterColumn('created_by', function($query, $keyword) {
                    $admins = Admin::whereRaw("name LIKE ?", ["%{$keyword}%"])->select('id')->get();
                    $admin_user_ids = [];
                    foreach ($admins as $admin) {
                        $admin_user_ids[] = $admin->id;
                    }
                    if(!empty($admin_user_ids))
                        $query->whereIn('created_by', $admin_user_ids);
                })
                ->editColumn('created_by', function($data) {
                    return $data->created_user->name;
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $html = '<a href="'. route('admin.companies.edit', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a><a href="'. route('admin.companies.users', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-users"></i>Users</a><a href="javascript:void(0);" onclick="openAccessModal('. $data->id .')" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-key"></i>Access</a><a href="javascript:void(0);" onclick="confirmModal('. $data->id .')" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</a>';
                    return $html;
                })
                ->rawColumns(['created_by', 'created_at', 'action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::company.create');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function createUser($company_id)
    {
        $company = Company::findOrFail($company_id);
        $roles = Role::get();
        return view('admin::company.createUser')->with('company', $company)->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'website' => ['max:255'],
            'user_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).+$/'],
        ],[
              'password.regex' => 'Password must contain at least 1 lower-case and capital letter, a number and symbol.',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {

            $company = Company::create([
                'created_by' => \Auth::guard('admin')->user()->id,
                'name' => $request->get('name'),
                'website' => $request->get('website')
            ]);

            $user = User::create([
                'name' => $request->get('user_name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'company_id' => $company->id
            ]);

            $adminRole = Role::where('name', '=', 'admin')->first();
            $user->attachRole($adminRole);

            try {
                $mail_data = ['user' => $user, 'password' => $request->get('password')];
                \Mail::to($request->get('email'))->send(new CompanyCreateEmailForDefaultUser($mail_data));
            } catch (\Exception $e) {
            }

            \Session::flash('message', 'Company added successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function storeUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role' => ['required', 'exists:roles,id'],
            'password' => ['required', 'confirmed', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).+$/'],
        ],[
              'password.regex' => 'Password must contain at least 1 lower-case and capital letter, a number and symbol.',
        ]);

        $company = Company::findOrFail($request->get('company_id'));
        $role = Role::findOrFail($request->get('role'));
        try {
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'company_id' => $company->id
            ]);

            $user->attachRole($role);

            try {
                $mail_data = ['user' => $user, 'password' => $request->get('password')];
                \Mail::to($request->get('email'))->send(new CompanyCreateEmailForDefaultUser($mail_data));
            } catch (\Exception $e) {
            }

            \Session::flash('message', 'User added successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies.users', ['id' => $request->get('company_id')]));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies.users', ['id' => $request->get('company_id')]));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        return view('admin::company.edit')->with('company', $company);
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function editUser($company_id, $id)
    {
        $company = Company::findOrFail($company_id);
        $roles = Role::get();
        $companies = Company::get();
        $user = $company->users()->where('id', '=', $id)->first();
        return view('admin::company.editUser')->with('company', $company)->with('user', $user)->with('companies', $companies)->with('roles', $roles);
    }
    

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'website' => 'max:255'
        ]);
        $company = Company::findOrFail($request->get('id'));
        try {
            Company::updateOrCreate([
                'id' => $company->id
            ], [
                'name' => $request->get('name'),
                'website' => $request->get('website')
            ]);
            \Session::flash('message', 'Company updated successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies'));
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function updateUser(Request $request)
    {
        $validate_array = [
            'name' => ['required', 'string', 'max:255'],
            'company' => ['required', 'exists:companies,id'],
            'role' => ['required', 'exists:roles,id']
        ];

        if(!empty($request->get('password')))
        {
            $validate_array['password'] = ['string', 'min:6', 'confirmed'];
        }

        $request->validate($validate_array);
        $company = Company::findOrFail($request->get('company_id'));
        $role = Role::findOrFail($request->get('role'));
        $user = $company->users()->where('id', '=', $request->get('id'))->first();
        try {
            $user->update([
                'name' => $request->get('name'),
                'company_id' => $request->get('company')
            ]);

            if(!$user->hasRole($role->name))
            {
                $user->detachRoles();
                $user->attachRole($role);
            }

            if(!empty($request->get('password')))
            {
                $user->password = Hash::make($request->get('password'));
                $user->save();

                try {
                    $mail_data = ['user' => $user, 'password' => $request->get('password')];
                    \Mail::to($user->email)->send(new UserPasswordChangeByAdmin($mail_data));
                } catch (\Exception $e) {
                }
            }

            \Session::flash('message', 'User updated successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies.users', ['id' => $request->get('company_id')]));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies.users', ['id' => $request->get('company_id')]));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $company = Company::findOrFail($request->get('id'));
        try {
            $company->delete();
            \Session::flash('message', 'Company deleted successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyUser(Request $request)
    {
        $company = Company::findOrFail($request->get('companyid'));
        try {
            $user = $company->users()->where('id', '=', $request->get('id'))->first();
            $user->delete();
            \Session::flash('message', 'User deleted successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('admin.companies.users', ['id' => $company->id]));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('admin.companies.users', ['id' => $company->id]));
        }
    }

    public function access(Request $request)
    {
        $courses = Course::get();
        $company_id = $request->get('company_id');

        $company = User::whereHas('roles', function ($query) use($company_id) {
                    $query->where('name', '=', 'admin');
                    $query->where('company_id', '=', $company_id);
         })->first();

        $course_ids = array();

        $courseAccess = CoursePermission::where('access_id', '=', $company->id)->select('course_id')->get();
        foreach ($courseAccess as $key => $value) {
            $course_ids[] = $value['course_id'];
        }

        return view('admin::company.partials.access')->with('courses',$courses)->with('company_id',$company_id)->with('course_ids', $course_ids);
    }

    public function storeAccess(Request $request)
    {
        $data = $request->all();
        $courses = array();
        if(!empty($data['courses']))
        {
            $courses = $data['courses'];
        }
        $company_id = $data['company_id'];
        $company = User::whereHas('roles', function ($query) use($company_id) {
                    $query->where('name', '=', 'admin');
                    $query->where('company_id', '=', $company_id);
         })->first();

        $users = User::whereHas('roles', function ($query) use($company_id) {
                    $query->where('name', '=', 'user');
                    $query->where('company_id', '=', $company_id);
         })->get();

        $coursenames = array();
        $course_ids = array();

        try {

            $courseAccessDelete = CoursePermission::where('access_id', '=', $company->id)->whereNotIn('course_id', $courses)->delete();

            foreach ($users as $user) {
                $usercourseAccessDelete = CoursePermission::where('access_id', '=', $user['id'])->whereNotIn('course_id', $courses)->delete();
            }

            $courseAccess = CoursePermission::where('access_id', '=', $company->id)->whereIn('course_id', $courses)->select('course_id')->get();
            foreach ($courseAccess as $key => $value) {
                $course_ids[] = $value['course_id'];
            }
            foreach ($courses as $course) {
                if(!in_array($course, $course_ids))
                {
                    $CoursePermission = CoursePermission::create([
                        'access_id' => $company->id,
                        'access_type' => "company",
                        'course_id' => $course
                    ]);
                    $courses = Course::where('id', '=', $course)->first();
                    $coursenames[] = $courses['name'];
                }
            }

            if(!empty($coursenames))
            {
                try {
                    $mail_data = ['coursenames' => $coursenames, 'name' => $company->name];
                    \Mail::to($company->email)->send(new CourseAccess($mail_data));
                }
                catch (\Exception $e) {
                }
            }

            $success['message'] =  "Course Permission added successfully.";
            return response()->json(['success'=>$success], 200);
        } catch (\Exception $e) {
            return response()->json(['error'=>'Something is wrong'], 401);
        }
    }

    public function usersAccess(Request $request)
    {
        $company_id = $request->get('company_id');

        $company = User::whereHas('roles', function ($query) use($company_id) {
                    $query->where('name', '=', 'admin');
                    $query->where('company_id', '=', $company_id);
         })->first();

        $user_id = $request->get('user_id');

        $course_ids = array();
        $usercourse_ids = array();

        $courseAccess = CoursePermission::where('access_id', '=', $company->id)->select('course_id')->get();
        foreach ($courseAccess as $value) {
            $course_ids[] = $value['course_id'];
        }

        $courses = Course::whereIn('id', $course_ids)->get();

        $userCourseAccess = CoursePermission::where('access_id', '=', $user_id)->select('course_id')->get();
        foreach ($userCourseAccess as $value) {
            $usercourse_ids[] = $value['course_id'];
        }

        return view('admin::company.partials.userAccess')->with('courses',$courses)->with('user_id',$user_id)->with('usercourse_ids', $usercourse_ids);
    }

    public function storeUsersAccess(Request $request)
    {
        $data = $request->all();
        $courses = array();
        if(!empty($data['courses']))
        {
            $courses = $data['courses'];
        }

        $user_id = $data['user_id'];
        $user = User::where('id', '=', $user_id)->first();

        $coursenames = array();
        $course_ids = array();

        try {

            $courseAccessDelete = CoursePermission::where('access_id', '=', $user->id)->whereNotIn('course_id', $courses)->delete();

            $courseAccess = CoursePermission::where('access_id', '=', $user->id)->whereIn('course_id', $courses)->select('course_id')->get();
            foreach ($courseAccess as $key => $value) {
                $course_ids[] = $value['course_id'];
            }
            foreach ($courses as $course) {
                if(!in_array($course, $course_ids))
                {
                    $CoursePermission = CoursePermission::create([
                        'access_id' => $user->id,
                        'access_type' => "user",
                        'course_id' => $course
                    ]);
                    $courses = Course::where('id', '=', $course)->first();
                    $coursenames[] = $courses['name'];
                }
            }

            if(!empty($coursenames))
            {
                try {
                    $mail_data = ['coursenames' => $coursenames, 'name' => $user->name];
                    \Mail::to($user->email)->send(new CourseAccess($mail_data));
                }
                catch (\Exception $e) {
                }
            }

            $success['message'] =  "Course Permission added successfully.";
            return response()->json(['success'=>$success], 200);
        } catch (\Exception $e) {
            return response()->json(['error'=>'Something is wrong'], 401);
        }
    }
}
