<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class AddDefaultAdminUserTableSeeder extends Seeder
{
    protected $adminUsers = [
        [
            'name' => 'Super Admin',
            'email' => 'admin@example.com',
            'password' => 'admin@123'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->adminUsers as $user) {
            if(!Admin::where('email', '=', $user['email'])->exists())
            {
                $superadmin = Admin::create(
                    [
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'password' => bcrypt($user['password'])
                    ]
                );
            }
        }
    }
}
