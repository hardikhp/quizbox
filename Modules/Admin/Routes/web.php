<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->middleware(['guest:admin'])->group(function() {
	Route::get('/login', 'AdminLoginController@login')->name('admin.auth.login');
	Route::post('/login', 'AdminLoginController@loginAdmin')->name('admin.auth.loginCheck');
});

Route::prefix('admin')->middleware(['auth:admin'])->group(function() {
  	Route::post('/logout', 'AdminLoginController@logout')->name('admin.auth.logout');
  	Route::get('/', 'AdminController@index')->name('admin.dashboard');

  	Route::get('/courses', 'CoursesController@index')->name('admin.courses');
  	Route::get('/courses/json', 'CoursesController@indexJson')->name('admin.courses.json');
  	Route::get('/courses/create', 'CoursesController@create')->name('admin.courses.create');
  	Route::post('/courses/store', 'CoursesController@store')->name('admin.courses.store');
  	Route::get('/courses/edit/{id}', 'CoursesController@edit')->name('admin.courses.edit');
  	Route::post('/courses/update', 'CoursesController@update')->name('admin.courses.update');
  	Route::post('/courses/delete', 'CoursesController@destroy')->name('admin.courses.delete');

    Route::get('/modules', 'ModulesController@index')->name('admin.modules');
    Route::get('/modules/json', 'ModulesController@indexJson')->name('admin.modules.json');
    Route::get('/modules/create', 'ModulesController@create')->name('admin.modules.create');
    
    Route::get('/modules/cameracapture', 'ModulesController@capture')->name('admin.modules.cameracapture');
    Route::post('/modules/store', 'ModulesController@store')->name('admin.modules.store');
    Route::get('/modules/edit/{id}', 'ModulesController@edit')->name('admin.modules.edit');
    Route::post('/modules/update', 'ModulesController@update')->name('admin.modules.update');
    Route::post('/modules/delete', 'ModulesController@destroy')->name('admin.modules.delete');

    Route::get('/results/model', 'ModulesController@modelData')->name('admin.results.modelData');
    Route::get('/results/{id}', 'ModulesController@results')->name('admin.modules.show');
    Route::get('/results/json/{id}', 'ModulesController@resultsJson')->name('admin.results.json');

  	Route::get('/companies', 'CompaniesController@index')->name('admin.companies');
  	Route::get('/companies/json', 'CompaniesController@indexJson')->name('admin.companies.json');
  	Route::get('/companies/create', 'CompaniesController@create')->name('admin.companies.create');
  	Route::post('/companies/store', 'CompaniesController@store')->name('admin.companies.store');
  	Route::get('/companies/edit/{id}', 'CompaniesController@edit')->name('admin.companies.edit');
  	Route::post('/companies/update', 'CompaniesController@update')->name('admin.companies.update');
  	Route::post('/companies/delete', 'CompaniesController@destroy')->name('admin.companies.delete');
  	Route::get('/companies/users/{id}', 'CompaniesController@users')->name('admin.companies.users');
  	Route::get('/companies/users/json/{id}', 'CompaniesController@usersJson')->name('admin.companies.users.json');
    Route::post('/companies/users/delete', 'CompaniesController@destroyUser')->name('admin.companies.users.delete');
  	Route::get('/companies/{company_id}/users/edit/{id}', 'CompaniesController@editUser')->name('admin.companies.users.edit');
  	Route::post('/companies/users/update', 'CompaniesController@updateUser')->name('admin.companies.users.update');
  	Route::get('/companies/{company_id}/users/create', 'CompaniesController@createUser')->name('admin.companies.users.create');
  	Route::post('/companies/users/store', 'CompaniesController@storeUser')->name('admin.companies.users.store');
    Route::get('/companies/access', 'CompaniesController@access')->name('admin.companies.access');
    Route::post('/companies/access', 'CompaniesController@storeAccess')->name('admin.companies.access');
    Route::get('/users/access', 'CompaniesController@usersAccess')->name('admin.users.access');
    Route::post('/users/access', 'CompaniesController@storeUsersAccess')->name('admin.users.access');

    Route::get('/exportpdf','ModulesController@exportPDF')->name('admin.exportpdf');
});