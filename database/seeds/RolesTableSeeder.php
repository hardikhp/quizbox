<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
	private $roles = [
		[
			'name' => 'admin',
			'display_name' => 'Admin',
			'description' => '',
		],
		[
			'name' => 'user',
			'display_name' => 'User',
			'description' => '',
		]
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
        	if(!Role::where('name', '=', $role['name'])->exists())
        	{
        		Role::create([
        			'name' => $role['name'],
        			'display_name' => $role['display_name'],
        			'description' => $role['description'],
        		]);
        	}
        }
    }
}