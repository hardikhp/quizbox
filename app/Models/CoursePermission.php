<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursePermission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access_id', 'access_type', 'course_id'
    ];
    
}