<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id', 'question'
    ];

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question_id');
    }

    public function video()
    {
        return $this->belongsTo('App\Models\Video', 'video_id');
    }
    
}