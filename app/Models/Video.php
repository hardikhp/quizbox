<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_id', 'name', 'slug', 'video', 'description', 'pass_percentage', 'meta_keywords', 'meta_description', 'upload_method'
    ];

    public function video_course()
    {
        return $this->belongsTo('App\Models\Course', 'course_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'video_id');
    }

    public function user_answers()
    {
        return $this->hasManyThrough('App\Models\UserAnswer', 'App\Models\Question', 'video_id', 'question_id', 'id', 'id');
    }
    
}