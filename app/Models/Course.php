<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'name', 'slug', 'description', 'meta_keywords', 'meta_description'
    ];
    
    /**
     * Get admin user that owns the course.
     */
    public function created_user()
    {
        return $this->belongsTo('Modules\Admin\Models\Admin', 'created_by');
    }
}