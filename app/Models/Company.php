<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'website', 'created_by'
    ];
    
    /**
     * Get admin user that owns the company.
     */
    public function created_user()
    {
        return $this->belongsTo('Modules\Admin\Models\Admin', 'created_by');
    }

    /**
     * Get the users for the company.
     */
    public function users()
    {
        return $this->hasMany('App\Models\User', 'company_id');
    }
}