<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'question_id', 'answer_id', 'question', 'answer', 'is_correct', 'retry_quiz'
    ];

    public function questions()
    {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }
    
}