<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class QuestionsAnswers implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = true;
        $datavalid = $this->data;

        $queanskeys = array();

        foreach ($datavalid['answers'] as $anskey => $ansvalue) {
            $answerkeys[] = $anskey;
        }

        foreach ($datavalid['questions'] as $quekey => $quevalue) {
            if(!in_array($quekey, $answerkeys))
            {
                $queanskeys[] = $quekey;
            }
        }
        if(count($queanskeys) > 0)
        {
            $value = false;
        }
        return $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter answers of all questions.';
    }
}
