<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SelectAnyAnswer implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = true;
        $datavalid = $this->data;
        $correctkeys = array();
        $correctkeys = $datavalid['is_correct'];

        foreach ($datavalid['answers'] as $anskey => $ansvalue) {
            if(!array_key_exists($anskey, $correctkeys))
            {
                $value = false;
            }
        }

        return $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please select answers of all questions.';
    }
}
