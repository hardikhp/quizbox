<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Company;
use App\Models\CoursePermission;
use App\Models\User;
use App\Models\Role;
use App\Models\Video;
use App\Models\Question;
use App\Models\Answer;
use App\Models\UserAnswer;
use App\Mail\CourseAccess;
use App\Mail\CompanyCreateEmailForDefaultUser;
use App\Mail\UserPasswordChangeByAdmin;
use Illuminate\Support\Facades\Hash;
use PDF;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function courseIndex()
    {
        return view('course');
    }

    public function courseIndexJson()
    {
        $loginuser = \Auth::user();
        $login_userid = $loginuser->id;
        //$company_id = $loginuser->company_id;

        $courses = Course::leftJoin('course_permissions', function($leftJoin) use($login_userid) {
        $leftJoin->on('courses.id', '=', 'course_permissions.course_id')
        ->on('course_permissions.access_id','=',\DB::raw($login_userid));})->whereNotNull('course_permissions.access_id')->where('course_permissions.access_type', '=', 'company')->orderBy('id', 'asc')->select('courses.*');

        return \DataTables::of($courses)
            ->filterColumn('created_at', function($query, $keyword) {
                if(substr_count($keyword, '/') == 2)
                {
                    $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                    if(!empty($date))
                    {
                        $query->whereDate('created_at', $date->format('Y-m-d'));
                    }
                }
            })
            ->filterColumn('created_by', function($query, $keyword) {
                $admins = Admin::whereRaw("name LIKE ?", ["%{$keyword}%"])->select('id')->get();
                $admin_user_ids = [];
                foreach ($admins as $admin) {
                    $admin_user_ids[] = $admin->id;
                }
                if(!empty($admin_user_ids))
                    $query->whereIn('created_by', $admin_user_ids);
            })
            ->editColumn('created_by', function($data) {
                return $data->created_user->name;
            })
            ->editColumn('created_at', function($data) {
                return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
            })
            ->editColumn('action', function($data) {
                $html = '<a href="javascript::void(0);" onclick="openAccessModal('. $data->id .')" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-key"></i>Access</a>';
                return $html;
            })
            ->rawColumns(['created_by', 'created_at', 'action'])
            ->make(true);
    }
    public function usersAccess(Request $request)
    {
        $loginuser = \Auth::user();

        $course_id = $request->get('course_id');

        $user_ids = array();

        $company_id = $loginuser->company_id;
        $users = User::whereHas('roles', function ($query) use($company_id) {
                    $query->where('name', '=', 'user');
                    $query->where('company_id', '=', $company_id);
         })->get();

        foreach ($users as $key => $user) {
            $coursepermission = CoursePermission::where('access_id', '=', $user->id)->where('course_id', '=', $course_id)->select('access_id')->first();
            $user_ids[] = $coursepermission['access_id'];
        }

        return view('layouts.partials.userAccess')->with('users',$users)->with('user_ids', $user_ids)->with('course_id',$course_id);
    }

    public function storeUsersAccess(Request $request)
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $data = $request->all();
        $users = array();
        $user_ids = array();

        if(!empty($data['users']))
        {
            $users = $data['users'];
        }

        $course_id = $data['course_id'];

        try {

            $courseAccessDelete = CoursePermission::leftJoin('users', function($leftJoin) use($company_id) {
                        $leftJoin->on('course_permissions.access_id', '=', 'users.id')
                        ->on('users.company_id','=',\DB::raw($company_id));})
                        ->where('course_permissions.access_type', '=', 'user')
                        ->where('course_permissions.course_id', '=', $course_id)
                        ->whereNotIn('access_id', $users)
                        ->select('course_permissions.access_id')
                        ->delete();

            $usersPermission = CoursePermission::leftJoin('users', function($leftJoin) use($company_id) {
                        $leftJoin->on('course_permissions.access_id', '=', 'users.id')
                        ->on('users.company_id','=',\DB::raw($company_id));})
                        ->where('course_permissions.access_type', '=', 'user')
                        ->where('course_permissions.course_id', '=', $course_id)
                        ->select('course_permissions.access_id')
                        ->get();

            foreach ($usersPermission as $key => $user) {
                $user_ids[] = $user['access_id'];
            }

            $course = Course::where('id', '=', $course_id)->first();
            $coursename = $course['name'];
            foreach ($users as $userid) {
                if(!in_array($userid, $user_ids))
                {
                    $CoursePermission = CoursePermission::create([
                        'access_id' => $userid,
                        'access_type' => "user",
                        'course_id' => $course_id
                    ]);

                    $user = User::where('id', '=', $userid)->first();

                    try {
                        $mail_data = ['coursename' => $coursename, 'name' => $user->name];
                        \Mail::to($user->email)->send(new CourseAccess($mail_data));
                    }
                    catch (\Exception $e) {
                    }
                }
            }

            $success['message'] =  "Course Permission added successfully.";
            return response()->json(['success'=>$success], 200);
        } catch (\Exception $e) {
            return response()->json(['error'=>'Something is wrong'], 401);
        }
    }

    public function moduleIndex()
    {
        return view('module');
    }

    public function moduleIndexJson()
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $course_ids = array();

        $courses = Course::leftJoin('course_permissions', function($leftJoin) use($company_id) {
        $leftJoin->on('courses.id', '=', 'course_permissions.course_id')
        ->on('course_permissions.access_id','=',\DB::raw($company_id));})->whereNotNull('course_permissions.access_id')->where('course_permissions.access_type', '=', 'company')->orderBy('id', 'asc')->select('courses.id')->get();

        foreach ($courses as $key => $value) {
            $course_ids[] = $value['id'];
        }

        $modules = Video::whereIn('course_id', $course_ids)->select('videos.*');

        return \DataTables::of($modules)
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->filterColumn('course_id', function($query, $keyword) {
                    $admins = Course::whereRaw("name LIKE ?", ["%{$keyword}%"])->select('id')->get();
                    $admin_user_ids = [];
                    foreach ($admins as $admin) {
                        $admin_user_ids[] = $admin->id;
                    }
                    if(!empty($admin_user_ids))
                        $query->whereIn('course_id', $admin_user_ids);
                })
                ->editColumn('course_id', function($data) {
                    return $data->video_course->name;
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $html = '<a href="'. route('modules.show', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-info"></i>Show</a><a href="'. route('modules.results', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-info"></i>Results</a>';
                    return $html;
                })
                ->rawColumns(['course_id', 'created_at', 'action'])
                ->make(true);
    }

    public function showModule($id)
    {
        $module = Video::findOrFail($id);
        $course = Course::where('id', '=', $module->course_id)->first();
        return view('showModule')->with('module',$module)->with('course',$course);
    }

    public function results($id)
    {
        return view('results')->with('id',$id);
    }

    public function resultsJson($id,Request $request)
    {
        $video_id = $id;
        $user_id = $request->userid;
        $retry_count = 0;
        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id');

        return \DataTables::of($user_results)
                ->filterColumn('user_id', function($query, $keyword) {
                })
                ->editColumn('Action',function($data){
                    $html = '<button type="button" class="btn btn-info btn-icon margin-r-5" onclick="modelOpen('.$data->user_id.');" ><i class="fa fa-info"></i>Result</button>';
                    $html .= '<a href="'. route('exportpdf', ['userid' => $data->user_id, 'video_id' => $data->video_id]) .'" class="btn btn-info btn-icon mr-rt-5"><i class="fa fa-download"></i> PDF</a>';
                    return $html;
                })
                ->editColumn('user_id', function($data) {
                    $user = User::find($data->user_id);
                    return $user['name'];
                })
                ->editColumn('Incorrect',function($data){
                    $in=$data['Total']-$data['correct'];
                    return $in;
                })
                ->editColumn('Percentage',function($data){
                    
                    $per=($data['correct']/$data['Total'])*100;
                    $per=round($per,2);
                    $per=$per.' '."%";
                    return $per;
                })
                 ->editColumn('Result',function($data){
                    $video_data=Video::find($data->video_id);
                    $percentage=$video_data['pass_percentage'];

                    $per=($data['correct']/$data['Total'])*100;
                    $per=round($per,2);
                    if($per>=$percentage){
                        return "<span style='color:green'>Pass</span>";
                    }else{
                        return "<span style='color:Red'>Fail</span>";
                    }
                })
                ->rawColumns(['user_id', 'company','Result','Action'])
                ->make(true);
    }
   

    public function users()
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $company = Company::findOrFail($company_id);
        return view('company.users')->with('company', $company);
    }

    public function usersJson()
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $companyusers = Company::findOrFail($company_id)->users()->where('id', '!=', $loginuser->id);
        return \DataTables::of($companyusers)
                ->filterColumn('created_at', function($query, $keyword) {
                    if(substr_count($keyword, '/') == 2)
                    {
                        $date = \DateTime::createFromFormat('d/m/Y', $keyword);
                        if(!empty($date))
                        {
                            $query->whereDate('created_at', $date->format('Y-m-d'));
                        }
                    }
                })
                ->editColumn('role', function($data) {
                    $roles = $data->roles()->get();
                    $roles_array = [];
                    foreach ($roles as $role) {
                        $roles_array[] = $role->display_name;
                    }
                    return implode(', ', $roles_array);
                })
                ->editColumn('created_at', function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->editColumn('action', function($data) {
                    $html = '<a href="'. route('users.edit', ['id' => $data->id]) .'" class="btn btn-info btn-icon margin-r-5"><i class="fa fa-edit"></i>Edit</a><a href="javascript::void(0);" onclick="confirmModal('. $data->id .')" class="btn btn-danger btn-icon margin-r-5"><i class="fa fa-trash"></i>Delete</a>';
                    return $html;
                })
                ->rawColumns(['role', 'created_at', 'action'])
                ->make(true);
    }

    public function createUser()
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $company = Company::findOrFail($company_id);
        $roles = Role::get();
        return view('company.createUser')->with('company', $company)->with('roles', $roles);
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role' => ['required', 'exists:roles,id'],
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ]);
        $company = Company::findOrFail($request->get('company_id'));
        $role = Role::findOrFail($request->get('role'));
        $loginuser = \Auth::user();
        $username = $loginuser->name;
        try {
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'company_id' => $company->id
            ]);

            $user->attachRole($role);

            try {
                $mail_data = ['user' => $user, 'username' => $username, 'companyname' => $company->name, 'password' => $request->get('password')];
                \Mail::to($request->get('email'))->send(new CompanyCreateEmailForDefaultUser($mail_data));
            } catch (\Exception $e) {
            }

            \Session::flash('message', 'User added successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('users'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('users'));
        }
    }

    public function editUser($id)
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $company = Company::findOrFail($company_id);
        $roles = Role::get();
        $user = $company->users()->where('id', '=', $id)->first();
        return view('company.editUser')->with('company', $company)->with('user', $user)->with('roles', $roles);
    }

    public function updateUser(Request $request)
    {
        $validate_array = [
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required', 'exists:roles,id']
        ];

        if(!empty($request->get('password')))
        {
            $validate_array['password'] = ['string', 'min:6', 'confirmed'];
        }

        $request->validate($validate_array);

        $loginuser = \Auth::user();
        $username = $loginuser->name;

        $company = Company::findOrFail($request->get('company_id'));
        $role = Role::findOrFail($request->get('role'));
        $user = $company->users()->where('id', '=', $request->get('id'))->first();
        try {
            $user->update([
                'name' => $request->get('name')
            ]);

            if(!$user->hasRole($role->name))
            {
                $user->detachRoles();
                $user->attachRole($role);
            }

            if(!empty($request->get('password')))
            {
                $user->password = Hash::make($request->get('password'));
                $user->save();

                try {
                    $mail_data = ['user' => $user, 'username' => $username, 'companyname' => $company->name, 'password' => $request->get('password')];
                    \Mail::to($user->email)->send(new UserPasswordChangeByAdmin($mail_data));
                } catch (\Exception $e) {
                }
            }

            \Session::flash('message', 'User updated successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('users'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('users'));
        }
    }

    public function destroyUser(Request $request)
    {
        $loginuser = \Auth::user();
        $company_id = $loginuser->company_id;

        $company = Company::findOrFail($company_id);
        try {
            $user = $company->users()->where('id', '=', $request->get('id'))->first();
            $user->delete();
            \Session::flash('message', 'User deleted successfully.');
            \Session::flash('alert-class', 'bg-success');
            return redirect(route('users'));
        } catch (\Exception $e) {
            \Session::flash('message', $e->getMessage());
            \Session::flash('alert-class', 'bg-danger');
            return redirect(route('users'));
        }
    }
     
    public function modelData(Request $request){
        $video_id = $request->video_id;
        $user_id = $request->userid;
        $user = User::find($user_id);
        $username = $user['name'];
        $company = Company::find($user['company_id']);
        $companyname = $company['name'];
        $retry_count = 0;

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($user_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id')->get();

        foreach ($user_results as $key => $value) {
            $correct = $value['correct'];
            $total = $value['Total'];
            $incorrect = $total - $correct;
            $percentage = ($correct/$total) * 100;

            $video_data = Video::find($video_id);
            $pass_percentage = $video_data['pass_percentage'];
            $percentage = round($percentage,2);

            if($percentage >= $pass_percentage){
                $results = "Pass";
            }else{
                $results = "Fail";
            }
        }

        $model_data = \DB::table('user_answers as ua')
                    ->select('ua.question','ua.answer','ua.is_correct')
                    ->join('questions as q','q.id','=','ua.question_id')
                    ->where('q.video_id',$video_id)
                    ->where('ua.user_id',$user_id)
                    ->where('ua.retry_quiz', '=', $retry_count)
                    ->get();
        
        $view = view('quizData',[
            'model_data' => $model_data,
            'correct' => $correct,
            'incorrect' => $incorrect,
            'percentage' => $percentage,
            'results' => $results,
            'retrycount' => $retry_count,
            'username' => $username,
            'companyname' => $companyname,
        ]);
        echo $view->render();


    }

    public function exportPDF(Request $request)
    {
        $video_id = $request->video_id;
        $user_id = $request->userid;
        $user = User::find($user_id);
        $username = $user['name'];
        $company = Company::find($user['company_id']);
        $companyname = $company['name'];
        $retry_count = 0;

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($video_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($user_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($video_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as Total'))
                    ->groupBy('user_answers.user_id')->get();

        foreach ($user_results as $key => $value) {
            $correct = $value['correct'];
            $total = $value['Total'];
            $incorrect = $total - $correct;
            $percentage = ($correct/$total) * 100;

            $video_data = Video::find($video_id);
            $pass_percentage = $video_data['pass_percentage'];
            $percentage = round($percentage,2);

            if($percentage >= $pass_percentage){
                $results = "Pass";
            }else{
                $results = "Fail";
            }
        }

        $model_data = \DB::table('user_answers as ua')
                    ->select('ua.question','ua.answer','ua.is_correct')
                    ->join('questions as q','q.id','=','ua.question_id')
                    ->where('q.video_id',$video_id)
                    ->where('ua.user_id',$user_id)
                    ->where('ua.retry_quiz', '=', $retry_count)
                    ->get();

        $pdf = PDF::loadView('exportpdf', compact('model_data','correct','incorrect','percentage','results','username','companyname','retry_count'));
        $pdf->save(storage_path().'_filename.pdf');
        //return $pdf->stream();
        return $pdf->download($user->name.'_'.$user_id.'.pdf');
        //return view('taxcalculations.exportpdf');
    }
}
