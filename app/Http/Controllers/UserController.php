<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Company;
use App\Models\CoursePermission;
use App\Models\User;
use App\Models\Role;
use App\Models\Video;
use App\Models\Question;
use App\Models\Answer;
use App\Models\UserAnswer;
use App\Mail\CourseAccess;
use App\Mail\CompanyCreateEmailForDefaultUser;
use App\Mail\UserPasswordChangeByAdmin;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function moduleIndex(Request $request)
    {
        $loginuser = \Auth::user();
        $user_id = $loginuser->id;

        $course_ids = array();

        $courses = Course::leftJoin('course_permissions', function($leftJoin) use($user_id) {
        $leftJoin->on('courses.id', '=', 'course_permissions.course_id')
        ->on('course_permissions.access_id','=',\DB::raw($user_id));})->whereNotNull('course_permissions.access_id')->where('course_permissions.access_type', '=', 'user')->orderBy('id', 'asc')->select('courses.id')->get();

        foreach ($courses as $key => $value) {
            $course_ids[] = $value['id'];
        }

        $totalcourses = Course::whereIn('id', $course_ids)->get();
        $coursesCount = count($totalcourses);

        $courses = Course::whereIn('id', $course_ids)->paginate(3);

        $modules = Video::whereIn('course_id', $course_ids)->select('videos.*')->get();

        return view('users.module')->with(compact('modules', 'courses', 'coursesCount'));
    }

    public function postmoduleIndex(Request $request)
    {
        $loginuser = \Auth::user();
        $user_id = $loginuser->id;

        $course_ids = array();

        $courses = Course::leftJoin('course_permissions', function($leftJoin) use($user_id) {
        $leftJoin->on('courses.id', '=', 'course_permissions.course_id')
        ->on('course_permissions.access_id','=',\DB::raw($user_id));})->whereNotNull('course_permissions.access_id')->where('course_permissions.access_type', '=', 'user')->orderBy('id', 'asc')->select('courses.id')->get();

        foreach ($courses as $key => $value) {
            $course_ids[] = $value['id'];
        }

        $id = $request->id;

        $courses = Course::whereIn('id', $course_ids)->orderBy('id','asc')->offset($id)->limit(3)->get();

        $modules = Video::whereIn('course_id', $course_ids)->select('videos.*')->get();

        if (!empty($courses) && $request->ajax()) {
            $view = view('users.ajaxmodule', compact('modules', 'courses'))->render();
            return response()->json(['html'=>$view]);
        }
    }

    public function showModule($id, $retry = "")
    {
        $loginuser = \Auth::user();
        $user_id = $loginuser->id;
        $retry_quiz = 0;
        $retry_count = 0;

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if(isset($user_answers['retry_count']) && $user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'), \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as totalques'))
                    ->groupBy('user_answers.user_id')->first();
        $answercount=0;
        if(isset($user_answers['total']) && $user_answers['total'] > 0)
        {
            $answercount = $user_answers['total'];
        }


        if($answercount == 0 && $retry != '')
        {
            return redirect(route('users.modules.show', ['id' => $id]));
        }

        if($answercount > 0)
        {
            $correct_answer = $user_answers['correct'];
            $total_questions = $user_answers['totalques'];
            $incorrect_answer = $total_questions - $correct_answer;
            $percentage = ($correct_answer/$total_questions)*100;
            $percentage = round($percentage,2);

            $video_data = Video::find($id);
            $passpercentage = $video_data['pass_percentage'];

            if($percentage >= $passpercentage)
            {
                return redirect(route('users.modules'));
            }

            if($retry == "" || $retry != "retry")
            {
                return redirect(route('users.modules.show', ['id' => $id, 'retry' => 'retry']));
            }
            $retry_quiz = $retry_count + 1;
        }

        $module = Video::findOrFail($id);
        $questions = $module->questions()->select('id')->get()->toArray();
        $questions = array_column($questions, 'id');

        $loginuser = \Auth::user();
        $user = User::findOrFail($loginuser->id);
        $user_answers = $user->user_answers()->select('question_id')->get()->toArray();

        $attendques = array();

        foreach ($user_answers as $key => $value) {
            if(in_array($value['question_id'], $questions))
            {
                $attendques[] = $value['question_id'];
            }
        }

        $attendQuesCount = sizeof($attendques);

        $course = Course::where('id', '=', $module->course_id)->first();
       // dd($user_answers,$questions,$attendques,$course);
        return view('users.showModule')->with('module',$module)->with('course',$course)->with('attendQuesCount',$attendQuesCount)->with('retry_quiz', $retry_quiz);
    }


    public function storeUserAnswers(Request $request)
    {
        $data = $request->all();
        $loginuser = \Auth::user();
        $user_id = $loginuser->id;
        $is_correct = 0;

        $retry_quiz = $data['retry_quiz'];

        $question = Question::findOrFail($data['question_id']);
        $answer = Answer::findOrFail($data['answer_id']);

        $coranswer = $question->answers->where('is_correct', '=', 1)->first();

        if($answer['is_correct'] == $coranswer['is_correct'])
        {
            $is_correct = 1;
        }
        try {
            $userAnswer = UserAnswer::create([
                'user_id' => $user_id,
                'question_id' => $data['question_id'],
                'answer_id' => $data['answer_id'],
                'question' => $question['question'],
                'answer' => $answer['answer'],
                'retry_quiz' => $retry_quiz,
                'is_correct' => $is_correct
            ]);
            $response = array(
                'status' => 'success',
                'message' => 'User answer added successfully.',
                'question' => $question['question'],
                'answer' => $answer['answer'],
                'answerstatus' => $is_correct,
            );
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = array(
                'status' => 'error',
                'message' => 'Something is wrong.',
            );
            return response()->json($response, 401);
        }
    }

    public function index(Request $request)
    {
        $loginuser = \Auth::user();
        $user_id = $loginuser->id;
        $user = User::findOrFail($user_id);

        $course_ids = array();

        $courses = Course::leftJoin('course_permissions', function($leftJoin) use($user_id) {
        $leftJoin->on('courses.id', '=', 'course_permissions.course_id')
        ->on('course_permissions.access_id','=',\DB::raw($user_id));})->whereNotNull('course_permissions.access_id')->where('course_permissions.access_type', '=', 'user')->orderBy('id', 'asc')->select('courses.id')->get();

        foreach ($courses as $key => $value) {
            $course_ids[] = $value['id'];
        }

        $modules = Video::whereIn('course_id', $course_ids)->select('videos.*')->get();
        $totalModules = count($modules);

        $user_questions = $user->user_answers()->groupBy('question_id')->pluck('question_id')->toArray();
        $modules = Question::whereIn('id', $user_questions)->groupBy('video_id')->select('video_id')->get();
        $attendModules = count($modules);

        $notattendedModules = $totalModules - $attendModules;

        return view('users.index')->with(compact('totalModules','attendModules','notattendedModules'));
    }

    public function getUserResult(Request $request)
    {
        $data = $request->all();

        $module_id = $data['id'];

        $loginuser = \Auth::user();
        $user_id = $loginuser->id;

        $retry_count = 0;

        $user_answers = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($module_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($module_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('MAX(user_answers.retry_quiz) as retry_count'), \DB::raw('count(user_answers.id) as total'))
                    ->groupBy('user_answers.user_id')->first();

        if($user_answers['retry_count'] > 0)
        {
            $retry_count = $user_answers['retry_count'];
        }

        $user_results = UserAnswer::leftJoin('questions', function($leftQueJoin) use ($module_id) {
                    $leftQueJoin->on('user_answers.question_id', '=', 'questions.id');})
                    ->where('questions.video_id', '=', \DB::raw($module_id))
                    ->where('user_answers.user_id', '=', \DB::raw($user_id))
                    ->where('user_answers.retry_quiz', '=', \DB::raw($retry_count))
                    ->select('user_answers.user_id','questions.video_id', \DB::raw('SUM(user_answers.is_correct) as correct'),\DB::raw('count(user_answers.is_correct) as total'))
                    ->groupBy('user_answers.user_id')->first();

        $correct_answer = $user_results['correct'];
        $total_questions = $user_results['total'];
        $incorrect_answer = $total_questions - $correct_answer;
        $percentage = ($correct_answer/$total_questions)*100;
        $percentage = round($percentage,2);
        $perc = $percentage.' '."%";

        $video_data = Video::find($module_id);
        $passpercentage = $video_data['pass_percentage'];

        if($percentage >= $passpercentage)
        {
            $result = 'Pass';
        }
        else
        {
            $result = 'Fail';
        }

        $response = array(
            'status' => 'success',
            'correct_answer' => $correct_answer,
            'incorrect_answer' => $incorrect_answer,
            'percentage' => $perc,
            'result' => $result,
            'total_questions' => $total_questions,
        );
        return response()->json($response, 200);
    }
}
