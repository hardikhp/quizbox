<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Auth::routes();*/

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    if(Auth::check())
    {
      if(Auth::user()->hasRole('admin'))
      {
        return redirect()->route('dashboard');
      }
      elseif (Auth::user()->hasRole('user')) {
        return redirect()->route('users.dashboard');
      }
    }
    else
    {
      return redirect(url('/login'));
    }
});

Auth::routes();

Route::group(['prefix' => 'company', 'middleware' => ['role:admin']], function() {

    Route::get('/home', 'HomeController@courseIndex')->name('dashboard');
    Route::get('/courses', 'HomeController@courseIndex')->name('courses');
    Route::get('/courses/json', 'HomeController@courseIndexJson')->name('courses.json');
    Route::get('/users/access', 'HomeController@usersAccess')->name('users.access');
    Route::post('/users/access', 'HomeController@storeUsersAccess')->name('users.access');
    Route::get('/modules', 'HomeController@moduleIndex')->name('modules');
    Route::get('/modules/json', 'HomeController@moduleIndexJson')->name('modules.json');
    Route::get('/modules/show/{id}', 'HomeController@showModule')->name('modules.show');

    Route::get('/results/model', 'HomeController@modelData')->name('results.modelData');
    Route::get('/results/{id}', 'HomeController@results')->name('modules.results');
    Route::get('/results/json/{id}', 'HomeController@resultsJson')->name('results.json');


    Route::get('/users', 'HomeController@users')->name('users');
    Route::get('/users/json', 'HomeController@usersJson')->name('users.json');

    Route::get('/users/create', 'HomeController@createUser')->name('users.create');
    Route::post('/users/store', 'HomeController@storeUser')->name('users.store');
    Route::get('/users/edit/{id}', 'HomeController@editUser')->name('users.edit');
    Route::post('users/update', 'HomeController@updateUser')->name('users.update');
    Route::post('/users/delete', 'HomeController@destroyUser')->name('users.delete');
    Route::get('/exportpdf','HomeController@exportPDF')->name('exportpdf');
});

Route::get('/logout', 'LoginController@logout')->name('auth.logout');

Route::group(['prefix' => 'user', 'middleware' => ['role:user']], function() {
    Route::get('/home', 'UserController@index')->name('users.dashboard');
    Route::get('/modules', 'UserController@moduleIndex')->name('users.modules');
    Route::post('/modules', 'UserController@postmoduleIndex')->name('users.modules');
    Route::get('/modules/json', 'UserController@moduleIndexJson')->name('users.modules.json');
    Route::get('/modules/show/{id}/{retry?}', 'UserController@showModule')->name('users.modules.show');
    Route::post('/answers/store', 'UserController@storeUserAnswers')->name('users.answers.store');
    Route::post('/result', 'UserController@getUserResult')->name('users.result');
});